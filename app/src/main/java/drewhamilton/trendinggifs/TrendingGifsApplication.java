package drewhamilton.trendinggifs;

import android.app.Application;
import com.facebook.drawee.backends.pipeline.Fresco;
import drewhamilton.trendinggifs.inject.AppComponent;
import drewhamilton.trendinggifs.inject.ComponentHolder;
import drewhamilton.trendinggifs.inject.DaggerAppComponent;
import drewhamilton.trendinggifs.logging.ReleaseTree;
import timber.log.Timber;

public class TrendingGifsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new ReleaseTree());
        }

        Fresco.initialize(this);

        final AppComponent appComponent = DaggerAppComponent.builder().build();
        ComponentHolder.setAppComponent(appComponent);
    }
}
