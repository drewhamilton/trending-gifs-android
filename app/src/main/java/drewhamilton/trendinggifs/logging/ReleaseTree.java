package drewhamilton.trendinggifs.logging;

import android.util.Log;
import timber.log.Timber;

public class ReleaseTree extends Timber.DebugTree {

    @Override
    protected boolean isLoggable(String tag, int priority) {
        return priority >= Log.INFO;
    }
}
