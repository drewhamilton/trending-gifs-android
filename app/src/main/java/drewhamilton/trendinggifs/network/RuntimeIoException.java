package drewhamilton.trendinggifs.network;

import java.io.IOException;

public class RuntimeIoException extends RuntimeException {

    public RuntimeIoException(String message, IOException cause) {
        super(message, cause);
    }
}
