package drewhamilton.trendinggifs.network.giphy;

import drewhamilton.trendinggifs.network.giphy.models.response.RandomResponse;
import retrofit2.Call;

import javax.inject.Inject;

public class RandomClient {

    private final GiphyApi api;
    private final Caller caller;

    @Inject
    RandomClient(GiphyApi api, Caller caller) {
        this.api = api;
        this.caller = caller;
    }

    public RandomResponse random() {
        Call<RandomResponse> call = api.random(Constants.API_KEY);
        return caller.execute(call);
    }
}
