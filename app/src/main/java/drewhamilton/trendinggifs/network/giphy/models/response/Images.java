package drewhamilton.trendinggifs.network.giphy.models.response;

public class Images {

    public final FixedWidthStillImage fixed_width_still;
    public final OriginalImage original;

    public Images(FixedWidthStillImage fixed_width_still, OriginalImage original) {
        this.fixed_width_still = fixed_width_still;
        this.original = original;
    }
}
