package drewhamilton.trendinggifs.network.giphy.models.response;

import com.squareup.moshi.Json;

public class RandomResponse {

    @Json(name = "data") public final Gif gif;

    public RandomResponse(Gif gif) {
        this.gif = gif;
    }
}
