package drewhamilton.trendinggifs.network.giphy;

import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;
import retrofit2.Call;

import javax.inject.Inject;

public class TrendingClient {

    private final GiphyApi api;
    private final Caller caller;

    @Inject
    TrendingClient(GiphyApi api, Caller caller) {
        this.api = api;
        this.caller = caller;
    }

    public TrendingResponse trending(int startIndex) {
        Call<TrendingResponse> call = api.trending(Constants.API_KEY, startIndex);
        return caller.execute(call);
    }
}
