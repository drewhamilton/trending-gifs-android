package drewhamilton.trendinggifs.network.giphy.models.response;

import drewhamilton.trendinggifs.network.tools.StringInt;

public class FixedWidthStillImage {

    public final String url;
    @StringInt public final int width;
    @StringInt public final int height;

    public FixedWidthStillImage(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }
}
