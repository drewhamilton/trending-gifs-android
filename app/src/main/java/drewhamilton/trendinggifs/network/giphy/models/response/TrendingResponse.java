package drewhamilton.trendinggifs.network.giphy.models.response;

import com.squareup.moshi.Json;

import java.util.List;

public class TrendingResponse {

    @Json(name = "data") public final List<Gif> gifs;
    public final Pagination pagination;

    public TrendingResponse(List<Gif> gifs, Pagination pagination) {
        this.gifs = gifs;
        this.pagination = pagination;
    }
}
