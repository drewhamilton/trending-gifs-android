package drewhamilton.trendinggifs.network.giphy;

import drewhamilton.trendinggifs.network.giphy.models.response.RandomResponse;
import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GiphyApi {

    @GET("/v1/gifs/trending")
    Call<TrendingResponse> trending(@Query("api_key") String apiKey, @Query("offset") int offset);

    @GET("/v1/gifs/random")
    Call<RandomResponse> random(@Query("api_key") String apiKey);
}
