package drewhamilton.trendinggifs.network.giphy.models.response;

public class Gif {

    public final String id;

    public final String title;
    public final Images images;

    public Gif(String id, String title, Images images) {
        this.id = id;
        this.title = title;
        this.images = images;
    }
}
