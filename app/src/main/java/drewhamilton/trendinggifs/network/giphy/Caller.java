package drewhamilton.trendinggifs.network.giphy;

import retrofit2.Call;
import timber.log.Timber;

import javax.inject.Inject;
import java.io.IOException;

class Caller {

    @Inject
    Caller() {}

    public <T> T execute(Call<T> call) {
        try {
            return call.execute().body();
        } catch (IOException ioException) {
            Timber.e("IOException when executing API call: %s", ioException.getMessage());
            throw new GiphyIoException(ioException);
        }
    }
}
