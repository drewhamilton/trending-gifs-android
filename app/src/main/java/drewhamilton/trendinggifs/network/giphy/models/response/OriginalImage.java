package drewhamilton.trendinggifs.network.giphy.models.response;

import drewhamilton.trendinggifs.network.tools.StringInt;

public class OriginalImage {

    public final String webp;
    @StringInt public final int width;
    @StringInt public final int height;

    public OriginalImage(String webPUrl, int width, int height) {
        this.webp = webPUrl;
        this.width = width;
        this.height = height;
    }
}
