package drewhamilton.trendinggifs.network.giphy.models.response;

public class Pagination {

    public final int offset;
    public final int count;
    public final int total_count;

    public Pagination(int offset, int count, int totalCount) {
        this.offset = offset;
        this.total_count = totalCount;
        this.count = count;
    }
}
