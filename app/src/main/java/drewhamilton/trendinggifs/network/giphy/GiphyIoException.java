package drewhamilton.trendinggifs.network.giphy;

import java.io.IOException;

import drewhamilton.trendinggifs.network.RuntimeIoException;

public class GiphyIoException extends RuntimeIoException {

    public GiphyIoException(IOException cause) {
        super("IOException while making call to the Giphy API", cause);
    }
}
