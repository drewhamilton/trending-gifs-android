package drewhamilton.trendinggifs.network.tools;

import com.squareup.moshi.JsonQualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used for data that is provided as a JSON string but should be an int
 */
@Retention(RetentionPolicy.RUNTIME)
@JsonQualifier
public @interface StringInt {}
