package drewhamilton.trendinggifs.network.tools;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

public class StringIntAdapter {

    @ToJson
    public String toJson(@StringInt int integer) {
        return String.valueOf(integer);
    }

    @FromJson
    @StringInt
    public int fromJson(String integerString) {
        return Integer.valueOf(integerString);
    }
}
