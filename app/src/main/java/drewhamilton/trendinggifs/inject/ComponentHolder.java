package drewhamilton.trendinggifs.inject;

public class ComponentHolder {

    private static AppComponent appComponent;

    private ComponentHolder() {}

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static void setAppComponent(AppComponent appComponent) {
        ComponentHolder.appComponent = appComponent;
    }
}
