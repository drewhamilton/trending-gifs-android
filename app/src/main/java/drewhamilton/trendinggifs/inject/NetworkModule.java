package drewhamilton.trendinggifs.inject;

import com.squareup.moshi.Moshi;
import dagger.Module;
import dagger.Provides;
import drewhamilton.trendinggifs.network.giphy.GiphyApi;
import drewhamilton.trendinggifs.network.tools.StringIntAdapter;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import javax.inject.Singleton;

@Module
class NetworkModule {

    @Provides
    Moshi provideMoshi() {
        return new Moshi.Builder()
                .add(new StringIntAdapter())
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideGiphyRetrofit(Moshi moshi) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .baseUrl("http://api.giphy.com")
                .build();
    }

    @Provides
    @Singleton
    GiphyApi provideGiphyApi(Retrofit giphyRetrofit) {
        return giphyRetrofit.create(GiphyApi.class);
    }
}
