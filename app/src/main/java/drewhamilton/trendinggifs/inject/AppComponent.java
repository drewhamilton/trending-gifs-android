package drewhamilton.trendinggifs.inject;

import dagger.Component;
import drewhamilton.trendinggifs.ui.random.RandomGifsActivity;
import drewhamilton.trendinggifs.ui.random.RandomGifsPresenter;
import drewhamilton.trendinggifs.ui.trending.TrendingGifsActivity;
import drewhamilton.trendinggifs.ui.trending.TrendingGifsPresenter;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        NetworkModule.class
})
public interface AppComponent {

    //region Presenters
    RandomGifsPresenter randomGifsPresenter();

    TrendingGifsPresenter trendingGifsPresenter();
    //endregion

    //region Inject Activities
    void inject(RandomGifsActivity randomGifsActivity);

    void inject(TrendingGifsActivity trendingGifsActivity);
    //endregion
}
