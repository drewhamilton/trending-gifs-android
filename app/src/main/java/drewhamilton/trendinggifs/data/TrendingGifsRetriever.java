package drewhamilton.trendinggifs.data;

import android.support.annotation.NonNull;
import drewhamilton.trendinggifs.data.models.AppModelFactory;
import drewhamilton.trendinggifs.data.models.PreviewPage;
import drewhamilton.trendinggifs.network.giphy.TrendingClient;
import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;

import javax.inject.Inject;

public class TrendingGifsRetriever {

    private final TrendingClient client;
    private final AppModelFactory modelFactory;

    @Inject
    TrendingGifsRetriever(TrendingClient client, AppModelFactory modelFactory) {
        this.client = client;
        this.modelFactory = modelFactory;
    }

    @NonNull
    public PreviewPage retrieveTrendingGifs(int startIndex) {
        TrendingResponse response = client.trending(startIndex);
        return modelFactory.newPreviewPage(response);
    }
}
