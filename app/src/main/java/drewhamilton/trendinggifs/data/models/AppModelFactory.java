package drewhamilton.trendinggifs.data.models;

import drewhamilton.trendinggifs.network.giphy.models.response.FixedWidthStillImage;
import drewhamilton.trendinggifs.network.giphy.models.response.Gif;
import drewhamilton.trendinggifs.network.giphy.models.response.OriginalImage;
import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class AppModelFactory {

    @Inject
    AppModelFactory() {}

    public PreviewPage newPreviewPage(TrendingResponse trendingResponse) {
        if (trendingResponse.gifs.size() != trendingResponse.pagination.count) {
            Timber.w("Network list size %d does not match network nominal count %d", trendingResponse.gifs.size(), trendingResponse.pagination.count);
        }

        List<GifItem> previewList = new ArrayList<>(trendingResponse.gifs.size());
        for (int i = 0; i < trendingResponse.gifs.size(); ++i) {
            Gif gif = trendingResponse.gifs.get(i);
            try {
                previewList.add(newPreviewImage(gif));
                Timber.v("Added item '%s' with overall index %d to page", gif.title, trendingResponse.pagination.offset + i);
            } catch (Exception exception) {
                Timber.w("Error converting item '%s' to app model; skipping it", gif.title);
            }
        }

        return new PreviewPage(previewList, trendingResponse.pagination.offset, trendingResponse.pagination.count);
    }

    public GifItem newPreviewImage(Gif gif) {
        final FixedWidthStillImage stillImage = gif.images.fixed_width_still;
        final OriginalImage originalImage = gif.images.original;
        return new GifItem(gif.title,
                stillImage.url, stillImage.width, stillImage.height,
                originalImage.webp, originalImage.width, originalImage.height);
    }
}
