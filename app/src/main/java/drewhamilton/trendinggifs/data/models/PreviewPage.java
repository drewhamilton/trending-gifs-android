package drewhamilton.trendinggifs.data.models;

import android.support.annotation.VisibleForTesting;

import java.util.List;

public class PreviewPage {

    public final List<GifItem> gifItems;
    public final int startIndex;

    /**
     * size represents the nominal server-side size; to be used for tracking pages. It may be different from gifPreviews.size() if there was
     * an error parsing one of the items in the list returned by the server.
     */
    public final int size;

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    public PreviewPage(List<GifItem> gifItems, int startIndex, int size) {
        this.gifItems = gifItems;
        this.startIndex = startIndex;
        this.size = size;
    }
}
