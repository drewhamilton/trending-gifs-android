package drewhamilton.trendinggifs.data.models;

import android.support.annotation.VisibleForTesting;

public class GifItem {

    public final String title;

    public final String stillUrl;
    public final int stillWidth;
    public final int stillHeight;

    public final String webPUrl;
    public final int webPWidth;
    public final int webPHeight;

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    public GifItem(String title, String stillUrl, int stillWidth, int stillHeight, String webPUrl, int webPWidth, int webPHeight) {
        this.title = title;
        this.stillUrl = stillUrl;
        this.stillWidth = stillWidth;
        this.stillHeight = stillHeight;
        this.webPUrl = webPUrl;
        this.webPWidth = webPWidth;
        this.webPHeight = webPHeight;
    }
}
