package drewhamilton.trendinggifs.data;

import android.support.annotation.NonNull;
import drewhamilton.trendinggifs.data.models.AppModelFactory;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.network.giphy.RandomClient;
import drewhamilton.trendinggifs.network.giphy.models.response.RandomResponse;

import javax.inject.Inject;

public class RandomGifRetriever {

    private final RandomClient client;
    private final AppModelFactory modelFactory;

    @Inject
    RandomGifRetriever(RandomClient client, AppModelFactory modelFactory) {
        this.client = client;
        this.modelFactory = modelFactory;
    }

    @NonNull
    public GifItem retrieveRandomGif() {
        RandomResponse response = client.random();
        return modelFactory.newPreviewImage(response.gif);
    }
}
