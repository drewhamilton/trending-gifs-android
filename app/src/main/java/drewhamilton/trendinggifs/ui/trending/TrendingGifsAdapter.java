package drewhamilton.trendinggifs.ui.trending;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.ui.images.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class TrendingGifsAdapter extends RecyclerView.Adapter<TrendingGifViewHolder> {

    private static final int VIEW_TYPE_PREVIEW = 1;
    private static final int VIEW_TYPE_EMPTY = 2;

    private static final int REQUEST_MORE_INTERVAL = 25;

    private final ImageLoader imageLoader;
    private final Callback callback;

    @NonNull private final List<PreviewViewModel> viewModels = new ArrayList<>();

    private boolean isShowingAllItems;

    TrendingGifsAdapter(ImageLoader imageLoader, Callback callback) {
        this.imageLoader = imageLoader;
        this.callback = callback;
    }

    @NonNull
    @Override
    public TrendingGifViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gif_preview, parent, false);
        return new TrendingGifViewHolder(itemView, imageLoader);
    }

    @Override
    public void onBindViewHolder(@NonNull TrendingGifViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_TYPE_PREVIEW:
                final PreviewViewModel viewModel = getViewModel(position);
                holder.bind(viewModel);
                holder.itemView.setOnClickListener(v -> callback.onItemClicked(viewModel));
                break;
            case VIEW_TYPE_EMPTY:
                // Don't ask for more items when loading position is 0; i.e. something is probably already loading items
                // Only ask for more items when binding positions that are multiples of 25
                // - To avoid asking for more items too many times
                // - To ensure items are requested periodically if user scrolls way fast to all empty items
                if (position > 0 && position % REQUEST_MORE_INTERVAL == 0) {
                  callback.requestMoreItems();
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return isShowingAllItems ? viewModels.size() : Integer.MAX_VALUE;
    }

    @Override
    public int getItemViewType(int position) {
        return position < viewModels.size() ? VIEW_TYPE_PREVIEW : VIEW_TYPE_EMPTY;
    }

    @NonNull
    List<PreviewViewModel> getViewModels() {
        return viewModels;
    }

    void addItems(@NonNull List<PreviewViewModel> newItems) {
        final int initialSize = viewModels.size();
        final int newItemsSize = newItems.size();
        viewModels.addAll(newItems);

        notifyItemRangeChanged(initialSize, newItemsSize);
    }

    void setShowingAllItems(boolean isShowingAllItems) {
        final boolean didChange = this.isShowingAllItems != isShowingAllItems;
        this.isShowingAllItems = isShowingAllItems;
        if (didChange) {
            // notifyItemRangeRemoved() causes a massive and seemingly permanent ANR for such a large range, so just re-bind the whole thing:
            notifyDataSetChanged();
        }
    }

    private PreviewViewModel getViewModel(int position) {
        return viewModels.get(position);
    }

    interface Callback {
        void requestMoreItems();

        void onItemClicked(PreviewViewModel viewModel);
    }
}
