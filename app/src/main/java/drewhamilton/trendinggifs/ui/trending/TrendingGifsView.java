package drewhamilton.trendinggifs.ui.trending;

import drewhamilton.trendinggifs.ui.base.MvpView;

import java.util.List;

public interface TrendingGifsView extends MvpView {

    void addItemsToDisplay(List<PreviewViewModel> previews);

    void displayEndOfItems();

    void launchRandomGifsScreen();
}
