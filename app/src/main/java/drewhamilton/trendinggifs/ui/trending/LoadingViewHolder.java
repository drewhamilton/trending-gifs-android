package drewhamilton.trendinggifs.ui.trending;

import android.view.View;

/**
 * Extends TrendingGifViewHolder as a convenience; but cannot actually display a TrendingGifViewModel
 */
class LoadingViewHolder extends TrendingGifViewHolder {

    LoadingViewHolder(View itemView) {
        super(itemView, null);
    }
}
