package drewhamilton.trendinggifs.ui.trending;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import drewhamilton.trendinggifs.data.TrendingGifsRetriever;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.data.models.PreviewPage;
import drewhamilton.trendinggifs.threading.AppSchedulers;
import drewhamilton.trendinggifs.ui.base.Presenter;
import drewhamilton.trendinggifs.ui.random.AnimatedViewModel;
import drewhamilton.trendinggifs.ui.random.RandomGifsScreenInitializationInformation;
import drewhamilton.trendinggifs.ui.random.RandomGifsScreenInitializer;
import io.reactivex.Single;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.*;

public class TrendingGifsPresenter extends Presenter<TrendingGifsView> {

    private final AppSchedulers appSchedulers;
    private final TrendingGifsRetriever retriever;
    private final RandomGifsScreenInitializer nextScreenInitializer;

    private int nextPageStartIndex = 0;

    @NonNull private final Map<String, AnimatedViewModel> animatedViewModelMap = new HashMap<>();

    @Inject
    TrendingGifsPresenter(AppSchedulers appSchedulers, TrendingGifsRetriever retriever, RandomGifsScreenInitializer nextScreenInitializer) {
        this.appSchedulers = appSchedulers;
        this.retriever = retriever;
        this.nextScreenInitializer = nextScreenInitializer;
    }

    @Override
    protected void attachView(@NonNull TrendingGifsView view) {
        super.attachView(view);
        Timber.v("Attached %s", view);

        nextPageStartIndex = 0;
        retrieveAndDisplayNextPageOfPreviews();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    protected void requestMorePreviews() {
        retrieveAndDisplayNextPageOfPreviews();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PACKAGE_PRIVATE)
    protected void launchSingleGifView(PreviewViewModel previewViewModel) {
        if (isViewAttached()) {
            final AnimatedViewModel animatedViewModel = animatedViewModelMap.get(previewViewModel.previewImageUrl);
            if (animatedViewModel == null) {
                Timber.w("Could not find animatedViewModel to pass to next screen");
            }

            nextScreenInitializer.provide(new RandomGifsScreenInitializationInformation(animatedViewModel));
            getView().launchRandomGifsScreen();
        }
    }

    private void retrieveAndDisplayNextPageOfPreviews() {
        addSubscription(Single.fromCallable(() -> retriever.retrieveTrendingGifs(nextPageStartIndex))
                .subscribeOn(appSchedulers.io())
                .retry(3)
                .observeOn(appSchedulers.ui())
                .doOnSuccess(page -> nextPageStartIndex += page.size)
                .map(TrendingGifsPresenter::convertToViewModelPairs)
                .doOnSuccess(this::cacheAnimatedViewModels)
                .map(TrendingGifsPresenter::convertToPreviewViewModels)
                .subscribe(
                        viewModels -> {
                            if (isViewAttached()) {
                                getView().addItemsToDisplay(viewModels);
                            }
                        },
                        error -> {
                            Timber.e("Error displaying items; not loading any more");
                            if (isViewAttached()) {
                                getView().displayEndOfItems();
                            }
                        }));
    }

    private void cacheAnimatedViewModels(Collection<ViewModelPair> viewModelPairs) {
        for (ViewModelPair viewModelPair : viewModelPairs) {
            animatedViewModelMap.put(viewModelPair.previewViewModel.previewImageUrl, viewModelPair.animatedViewModel);
        }
    }

    private static List<ViewModelPair> convertToViewModelPairs(PreviewPage previewPage) {
        final List<ViewModelPair> viewModelPairs = new ArrayList<>(previewPage.gifItems.size());
        for (GifItem gifItem : previewPage.gifItems) {
            final PreviewViewModel previewViewModel = new PreviewViewModel(gifItem.title, gifItem.stillUrl);
            final AnimatedViewModel animatedViewModel = new AnimatedViewModel(gifItem.title, gifItem.webPUrl);
            viewModelPairs.add(new ViewModelPair(previewViewModel, animatedViewModel));
        }

        return viewModelPairs;
    }

    private static List<PreviewViewModel> convertToPreviewViewModels(List<ViewModelPair> viewModelPairs) {
        List<PreviewViewModel> viewModels = new ArrayList<>(viewModelPairs.size());
        for (ViewModelPair viewModelPair : viewModelPairs) {
            viewModels.add(viewModelPair.previewViewModel);
        }

        return viewModels;
    }

    private static class ViewModelPair {

        final PreviewViewModel previewViewModel;
        final AnimatedViewModel animatedViewModel;

        ViewModelPair(PreviewViewModel previewViewModel, AnimatedViewModel animatedViewModel) {
            this.previewViewModel = previewViewModel;
            this.animatedViewModel = animatedViewModel;
        }
    }
}
