package drewhamilton.trendinggifs.ui.trending;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewTreeObserver;
import butterknife.BindView;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.inject.ComponentHolder;
import drewhamilton.trendinggifs.ui.base.BaseActivity;
import drewhamilton.trendinggifs.ui.base.DataRetainer;
import drewhamilton.trendinggifs.ui.images.ImageLoader;
import drewhamilton.trendinggifs.ui.random.RandomGifsActivity;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.List;

public class TrendingGifsActivity
        extends BaseActivity<TrendingGifsView, TrendingGifsPresenter>
        implements TrendingGifsView, TrendingGifsAdapter.Callback {

    private static final String KEY_LIST_POSITION = TrendingGifsActivity.class.getCanonicalName() + "+LIST_POSITION";

    @Inject ImageLoader imageLoader;

    @BindView(R.id.list)
    RecyclerView listView;

    private int targetPreviewWidthDp;

    private DataRetainer<List<PreviewViewModel>> dataRetainer;

    private TrendingGifsAdapter adapter;

    @Override
    protected int getLayout() {
        return R.layout.activity_trending_gifs;
    }

    @Override
    protected TrendingGifsPresenter newPresenter() {
        return ComponentHolder.getAppComponent().trendingGifsPresenter();
    }

    //region Lifecycle
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ComponentHolder.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        // Make preview images close to square by setting target width equal to height:
        targetPreviewWidthDp = convertToDp(getResources().getDimensionPixelSize(R.dimen.height_preview));

        dataRetainer = DataRetainer.initialize(getSupportFragmentManager(), DataRetainer.DEFAULT_TAG);

        restoreSavedViewModels();

        listView.setAdapter(lazyGetAdapter());
        listView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                listView.setLayoutManager(newLayoutManager());
                if (savedInstanceState != null) {
                    restoreSavedPosition(savedInstanceState);
                }
                listView.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveViewModels();
        saveListPosition(outState);
    }
    //endregion

    //region TrendingGifsView
    @Override
    public void addItemsToDisplay(List<PreviewViewModel> previews) {
        lazyGetAdapter().addItems(previews);
    }

    @Override
    public void displayEndOfItems() {
        lazyGetAdapter().setShowingAllItems(true);
    }

    @Override
    public void launchRandomGifsScreen() {
        startActivity(new Intent(this, RandomGifsActivity.class));
    }
    //endregion

    //region TrendingGifsAdapter.Callback
    @Override
    public void requestMoreItems() {
        getPresenter().requestMorePreviews();
    }

    @Override
    public void onItemClicked(PreviewViewModel viewModel) {
        getPresenter().launchSingleGifView(viewModel);
    }
    //endregion

    private void saveViewModels() {
        final List<PreviewViewModel> viewModels = adapter.getViewModels();
        dataRetainer.setData(viewModels);
    }

    private void saveListPosition(@NonNull Bundle outState) {
        final LinearLayoutManager layoutManager = (LinearLayoutManager) listView.getLayoutManager();
        if (layoutManager != null) {
            outState.putInt(KEY_LIST_POSITION, layoutManager.findFirstCompletelyVisibleItemPosition());
        }
    }

    private void restoreSavedViewModels() {
        final List<PreviewViewModel> restoredModels = dataRetainer.getData();
        if (restoredModels != null) {
            lazyGetAdapter().addItems(restoredModels);
        }
    }

    private void restoreSavedPosition(@NonNull Bundle savedInstanceState) {
        int savedPosition = savedInstanceState.getInt(KEY_LIST_POSITION, 0);
        listView.scrollToPosition(savedPosition);
    }

    private TrendingGifsAdapter lazyGetAdapter() {
        if (adapter == null) {
            adapter = new TrendingGifsAdapter(imageLoader, this);
        }
        return adapter;
    }

    private RecyclerView.LayoutManager newLayoutManager() {
        final int listViewWidthDp = convertToDp(listView.getWidth());
        Timber.v("listView width in dp: %d", listViewWidthDp);

        final int columnCount = listViewWidthDp / targetPreviewWidthDp;
        Timber.v("Calculated column count: %d", columnCount);
        return new GridLayoutManager(listView.getContext(), Math.max(1, columnCount));
    }
}
