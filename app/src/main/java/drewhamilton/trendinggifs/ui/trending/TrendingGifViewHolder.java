package drewhamilton.trendinggifs.ui.trending;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.facebook.drawee.view.GenericDraweeView;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.ui.images.ImageLoader;

class TrendingGifViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.preview_image)
    @Nullable GenericDraweeView previewImageView;

    private final ImageLoader imageLoader;

    TrendingGifViewHolder(View itemView, ImageLoader imageLoader) {
        super(itemView);
        this.imageLoader = imageLoader;
        ButterKnife.bind(this, itemView);
    }

    void bind(PreviewViewModel viewModel) {
        if (previewImageView == null) {
            throw new UnsupportedOperationException("This ViewHolder does not have an ImageView; so it can't bind a TrendingGifViewModel");
        }

        previewImageView.setContentDescription(viewModel.description);
        imageLoader.loadPreviewImage(previewImageView, viewModel.previewImageUrl, null);
    }
}
