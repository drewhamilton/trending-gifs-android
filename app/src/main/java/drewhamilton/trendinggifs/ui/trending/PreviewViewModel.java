package drewhamilton.trendinggifs.ui.trending;

import java.io.Serializable;

/**
 * Serializable only for config changes. Should not be persisted in a serialized state
 */
class PreviewViewModel implements Serializable {

    final String description;
    final String previewImageUrl;

    PreviewViewModel(String description, String previewImageUrl) {
        this.description = description;
        this.previewImageUrl = previewImageUrl;
    }
}
