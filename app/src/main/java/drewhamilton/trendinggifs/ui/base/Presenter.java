package drewhamilton.trendinggifs.ui.base;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class Presenter<V extends MvpView> {

    private V view;

    private final CompositeDisposable subscriptions = new CompositeDisposable();

    /**
     * Attach a new View
     *
     * @throws ViewAlreadyAttachedException if this Presenter already has a View attached
     */
    protected void attachView(@NonNull V view) {
        if (isViewAttached()) {
            throw new ViewAlreadyAttachedException();
        } else {
            this.view = view;
        }
    }

    /**
     * Replace an existing view; e.g. when the configuration changes and the system spins up a new Activity
     * Does *not* call detachView(); so subscriptions stay active
     *
     * @throws NoViewToReplaceException if this Presenter does not already have a view attached
     */
    protected void replaceView(@NonNull V view) {
        if (isViewAttached()) {
            this.view = view;
        } else {
            throw new NoViewToReplaceException();
        }
    }

    protected void detachView() {
        clearSubscriptions();
        this.view = null;
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    protected V getView() {
        return view;
    }

    protected final void addSubscription(Disposable subscription) {
        subscriptions.add(subscription);
    }

    protected final void clearSubscriptions() {
        subscriptions.clear();
    }

    public static class ViewAlreadyAttachedException extends RuntimeException {

        ViewAlreadyAttachedException() {
            super("Tried to attach a view when another view was already attached");
        }
    }

    public static class NoViewToReplaceException extends RuntimeException {

        NoViewToReplaceException() {
            super("Tried to replace a view when there was no view to replace");
        }
    }
}
