package drewhamilton.trendinggifs.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import butterknife.ButterKnife;

public abstract class BaseActivity<V extends MvpView, P extends Presenter<V>>
        extends AppCompatActivity {

    protected static final long DURATION_ANIMATION = 250L;

    private static final int NIGHT_MODE = AppCompatDelegate.MODE_NIGHT_YES;

    private P presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);

        AppCompatDelegate.setDefaultNightMode(NIGHT_MODE);

        attachToPresenter(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        // Only detach from Presenter if this is a "real" destroy rather than a config change:
        if (!isChangingConfigurations()) {
            presenter.detachView();
        }
        super.onDestroy();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return getPresenter();
    }

    @LayoutRes
    protected abstract int getLayout();

    protected abstract P newPresenter();

    protected P getPresenter() {
        return presenter;
    }

    protected int convertToDp(int px) {
        return (int) (px / getResources().getDisplayMetrics().density);
    }

    private void attachToPresenter(@Nullable Bundle savedInstanceState) {
        final V view;
        try {
            view = thisAsView();
        } catch (ClassCastException ex) {
            throw new InvalidViewImplementationException();
        }

        presenter = (P) getLastCustomNonConfigurationInstance();
        if (presenter == null) {
            presenter = newPresenter();
            presenter.attachView(view);
        } else {
            presenter.replaceView(view);
        }
    }

    private V thisAsView() {
        return (V) this;
    }

    private static class InvalidViewImplementationException extends RuntimeException {

        InvalidViewImplementationException() {
            super("All BaseActivity subclasses must implement the MvpView type by which they are parameterized");
        }
    }
}
