package drewhamilton.trendinggifs.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 *
 * This class exists to store large amounts of View-layer data on a configuration change. It should NOT be used as an actual UI fragment.
 *
 * @param <D> The data type being retained. Make a container object if you are saving more than 1 piece of data.
 */
public class DataRetainer<D> {

    public static final String DEFAULT_TAG = DataRetainer.class.getSimpleName();

    private DataFragment<D> dataFragment;

    public static <D> DataRetainer<D> initialize(FragmentManager fragmentManager, String tag) {
        DataRetainer<D> dataRetainer = new DataRetainer<>();
        dataRetainer.dataFragment = (DataFragment<D>) fragmentManager.findFragmentByTag(tag);

        if (dataRetainer.dataFragment == null) {
            dataRetainer.dataFragment = new DataFragment<>();
            fragmentManager.beginTransaction()
                    .add(dataRetainer.dataFragment, tag)
                    .commit();
        }

        return dataRetainer;
    }

    public D getData() {
        return dataFragment.data;
    }

    public void setData(D data) {
        dataFragment.data = data;
    }

    @VisibleForTesting // Actually visible for Android, but should not be used directly in production
    public static class DataFragment<D> extends Fragment {

        private D data;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }
    }
}
