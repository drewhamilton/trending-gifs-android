package drewhamilton.trendinggifs.ui.base;

import timber.log.Timber;

/**
 * Used to pass data from one {@link Presenter} to a new {@link Presenter}. Since Presenters are created in the View layer, it can't be done
 * directly (except using Intent extras, which have various limitations). This class stores the needed information as its state, but behaves
 * pseudo-statelessly by resetting its information to null as soon as that information is retrieved.
 *
 * Subclasses must be singletons and need to define {@link I}.
 */
public abstract class ScreenInitializer<I extends InitializationInformation> {

    private I information;

    public final void provide(I information) {
        if (this.information != null) {
            Timber.w("Previous information was never retrieved and will be lost: %s", this.information);
        }

        Timber.v("Storing %s for retrieving Presenter to retrieve", information);
        this.information = information;
    }

    public final I retrieve() {
        Timber.v("Retrieving %s and resetting state", information);
        final I temp = information;
        information = null;
        return temp;
    }
}
