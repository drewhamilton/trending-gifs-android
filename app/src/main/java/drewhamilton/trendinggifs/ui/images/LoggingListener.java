package drewhamilton.trendinggifs.ui.images;

import android.graphics.drawable.Animatable;
import android.support.annotation.Nullable;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;
import timber.log.Timber;

class LoggingListener extends BaseControllerListener<ImageInfo> {

    private final String url;

    LoggingListener(String url) {
        this.url = url;
    }

    //region ControllerListener
    @Override
    public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
        logSuccess();
    }

    @Override
    public void onFailure(String id, Throwable throwable) {
        logFailure(throwable);
    }
    //endregion

    private void logSuccess() {
        Timber.v("Successfully loaded %s", url);
    }

    private void logFailure(@Nullable Throwable e) {
        Timber.w("Failed to load %s: %s", url, e == null ? "null" : e.getMessage());
    }
}
