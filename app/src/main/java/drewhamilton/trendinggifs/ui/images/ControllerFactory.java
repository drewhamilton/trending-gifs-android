package drewhamilton.trendinggifs.ui.images;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.image.ImageInfo;

import javax.inject.Inject;

class ControllerFactory {

    @Inject
    ControllerFactory() {}

    public DraweeController buildDraweeController(String url, DraweeController oldController, boolean isAnimated,
            ControllerListener<ImageInfo> listener) {
        return Fresco.newDraweeControllerBuilder()
                .setUri(url)
                .setOldController(oldController)
                .setAutoPlayAnimations(isAnimated)
                .setControllerListener(listener)
                .build();
    }
}
