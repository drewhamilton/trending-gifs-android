package drewhamilton.trendinggifs.ui.images;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.controller.ForwardingControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import drewhamilton.trendinggifs.R;

import javax.inject.Inject;

public class ImageLoader {

    @DrawableRes
    private static final int FALLBACK_DRAWABLE = R.drawable.ic_error_white_24dp;

    private final ControllerFactory controllerFactory;

    @Inject
    public ImageLoader(ControllerFactory controllerFactory) {
        this.controllerFactory = controllerFactory;
    }

    public void loadPreviewImage(GenericDraweeView imageView, String stillImageUrl, @Nullable ControllerListener<ImageInfo> listener) {
        loadImage(imageView, stillImageUrl, ScalingUtils.ScaleType.CENTER_CROP, false, listener);
    }

    public void loadAnimatedImage(GenericDraweeView imageView, String animatedImageUrl, @Nullable ControllerListener<ImageInfo> listener) {
        loadImage(imageView, animatedImageUrl, ScalingUtils.ScaleType.FIT_CENTER, true, listener);
    }

    private void loadImage(GenericDraweeView imageView, String url, ScalingUtils.ScaleType scaleType, boolean isAnimated,
                @Nullable ControllerListener<ImageInfo> listener) {

        final LoggingListener loggingListener = new LoggingListener((url));
        final ControllerListener<ImageInfo> actualListener;
        if (listener == null) {
            actualListener = loggingListener;
        } else {
            actualListener = ForwardingControllerListener.of(loggingListener, listener);
        }

        setUpFailureImage(imageView);
        imageView.getHierarchy().setActualImageScaleType(scaleType);
        imageView.setController(controllerFactory.buildDraweeController(url, imageView.getController(), isAnimated, actualListener));
    }

    private static void setUpFailureImage(GenericDraweeView imageView) {
        imageView.getHierarchy().setFailureImage(FALLBACK_DRAWABLE, ScalingUtils.ScaleType.CENTER_INSIDE);
    }
}
