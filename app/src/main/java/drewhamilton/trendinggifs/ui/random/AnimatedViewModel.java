package drewhamilton.trendinggifs.ui.random;

import java.io.Serializable;

public class AnimatedViewModel implements Serializable {

    public final String description;
    public final String animatedImageUrl;

    public AnimatedViewModel(String description, String animatedImageUrl) {
        this.description = description;
        this.animatedImageUrl = animatedImageUrl;
    }
}
