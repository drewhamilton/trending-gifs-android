package drewhamilton.trendinggifs.ui.random;

import android.support.annotation.NonNull;
import drewhamilton.trendinggifs.ui.base.MvpView;

public interface RandomGifsView extends MvpView {

    void displayNewGif(@NonNull AnimatedViewModel viewModel);
}
