package drewhamilton.trendinggifs.ui.random;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.inject.ComponentHolder;
import drewhamilton.trendinggifs.ui.base.BaseActivity;
import drewhamilton.trendinggifs.ui.images.ImageLoader;
import timber.log.Timber;

import javax.inject.Inject;

public class RandomGifsActivity
        extends BaseActivity<RandomGifsView, RandomGifsPresenter>
        implements RandomGifsView {

    private static final String KEY_SAVED_VIEW_MODEL = RandomGifsActivity.class.getCanonicalName() + "+SAVED_VIEW_MODEL";

    private static final float ALPHA_ACTIVE_VIEW = 1f;
    private static final float ALPHA_INACTIVE_VIEW = 0f;

    @Inject ImageLoader imageLoader;

    @BindView(R.id.progress_circle)
    View progressCircle;

    @BindView(R.id.random_gif_a)
    GenericDraweeView imageViewA;

    @BindView(R.id.random_gif_b)
    GenericDraweeView imageViewB;

    private AnimatedViewModel activeViewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_random_gifs;
    }

    @Override
    protected RandomGifsPresenter newPresenter() {
        return ComponentHolder.getAppComponent().randomGifsPresenter();
    }

    //region Lifecycle
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ComponentHolder.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        restoreSavedViewModel(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().resumeView();
    }

    @Override
    protected void onPause() {
        getPresenter().pauseView();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_SAVED_VIEW_MODEL, activeViewModel);
    }
    //endregion

    //region RandomGifsView
    @Override
    public void displayNewGif(@NonNull AnimatedViewModel viewModel) {
        final ActionBar actionBar = getSupportActionBar();
        final GenericDraweeView activeImageView = getActiveImageView();
        final GenericDraweeView inactiveImageView = getInactiveImageView();

        inactiveImageView.setContentDescription(viewModel.description);
        imageLoader.loadAnimatedImage(inactiveImageView, viewModel.animatedImageUrl, new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable animatable) {
                Timber.v("Finished loading %s for display in %s", id, getImageViewName(inactiveImageView));
                activeViewModel = viewModel;

                if (actionBar != null) {
                    actionBar.setTitle(viewModel.description);
                }

                fadeOut(activeImageView, () -> activeImageView.setController(null));
                fadeIn(inactiveImageView, () -> progressCircle.setVisibility(View.GONE));
            }
        });
    }
    //endregion

    private GenericDraweeView getActiveImageView() {
        return imageViewA.getAlpha() == 1f ? imageViewA : imageViewB;
    }

    private GenericDraweeView getInactiveImageView() {
        return getActiveImageView() == imageViewA ? imageViewB : imageViewA;
    }

    private String getImageViewName(ImageView imageView) {
        if (imageView == imageViewA) {
            return "imageViewA";
        } else if (imageView == imageViewB) {
            return "imageViewB";
        } else {
            return "neither";
        }
    }

    private void restoreSavedViewModel(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            AnimatedViewModel savedViewModel = (AnimatedViewModel) savedInstanceState.getSerializable(KEY_SAVED_VIEW_MODEL);
            if (savedViewModel != null) {
                Timber.v("Restoring saved view model: %s", savedViewModel.description);
                displayNewGif(savedViewModel);
            }
        }
    }

    private static void fadeOut(View view, Runnable endAction) {
        view.animate()
                .alpha(ALPHA_INACTIVE_VIEW)
                .setDuration(DURATION_ANIMATION)
                .withEndAction(endAction)
                .start();
    }

    private static void fadeIn(View view, Runnable endAction) {
        view.animate()
                .alpha(ALPHA_ACTIVE_VIEW)
                .setDuration(DURATION_ANIMATION)
                .withEndAction(endAction)
                .start();
    }
}
