package drewhamilton.trendinggifs.ui.random;

import android.support.annotation.NonNull;
import drewhamilton.trendinggifs.data.RandomGifRetriever;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.threading.AppSchedulers;
import drewhamilton.trendinggifs.ui.base.Presenter;
import io.reactivex.Observable;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class RandomGifsPresenter extends Presenter<RandomGifsView> {

    private static final int NEW_IMAGE_FREQUENCY_SECONDS = 10;

    private final RandomGifsScreenInitializer initializer;
    private final AppSchedulers appSchedulers;
    private final RandomGifRetriever retriever;

    @Inject
    RandomGifsPresenter(RandomGifsScreenInitializer initializer, AppSchedulers appSchedulers, RandomGifRetriever retriever) {
        this.initializer = initializer;
        this.appSchedulers = appSchedulers;
        this.retriever = retriever;
    }

    @Override
    protected void attachView(@NonNull RandomGifsView view) {
        super.attachView(view);
        final RandomGifsScreenInitializationInformation initializationInformation = initializer.retrieve();
        final AnimatedViewModel initialViewModel;
        if (initializationInformation == null || initializationInformation.viewModel == null) {
            initialViewModel = new ErrorAnimatedViewModel();
        } else {
            initialViewModel = initializationInformation.viewModel;
        }
        displayNewGif(initialViewModel);

        subscribeToNewGifs();
    }

    protected void resumeView() {
        subscribeToNewGifs();
    }

    protected void pauseView() {
        clearSubscriptions();
    }

    private void subscribeToNewGifs() {
        // Clear subscriptions to ensure we don't get duplicate image subscriptions:
        clearSubscriptions();

        addSubscription(Observable.interval(NEW_IMAGE_FREQUENCY_SECONDS, TimeUnit.SECONDS, appSchedulers.io())
                .subscribeOn(appSchedulers.io())
                .flatMap(iteration -> Observable.fromCallable(retriever::retrieveRandomGif)
                    .doOnError(error -> Timber.e("Error retrieving random GIF: %s", error.getMessage()))
                    .retry(2))
                .observeOn(appSchedulers.ui())
                .map(RandomGifsPresenter::convertToAnimatedViewModel)
                .subscribe(this::displayNewGif, error -> displayNewGif(new ErrorAnimatedViewModel())));
    }

    private void displayNewGif(@NonNull AnimatedViewModel viewModel) {
        if (isViewAttached()) {
            Timber.v("Displaying new GIF: %s", viewModel.description);
            getView().displayNewGif(viewModel);
        }
    }

    private static AnimatedViewModel convertToAnimatedViewModel(GifItem gifItem) {
        return new AnimatedViewModel(gifItem.title, gifItem.webPUrl);
    }
}
