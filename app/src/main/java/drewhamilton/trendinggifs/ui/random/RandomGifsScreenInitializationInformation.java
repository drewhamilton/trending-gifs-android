package drewhamilton.trendinggifs.ui.random;

import drewhamilton.trendinggifs.ui.base.InitializationInformation;

public class RandomGifsScreenInitializationInformation implements InitializationInformation {

    public final AnimatedViewModel viewModel;

    public RandomGifsScreenInitializationInformation(AnimatedViewModel viewModel) {
        this.viewModel = viewModel;
    }
}
