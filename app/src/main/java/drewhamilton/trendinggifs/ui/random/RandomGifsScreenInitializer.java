package drewhamilton.trendinggifs.ui.random;

import drewhamilton.trendinggifs.ui.base.ScreenInitializer;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RandomGifsScreenInitializer extends ScreenInitializer<RandomGifsScreenInitializationInformation> {

    @Inject
    RandomGifsScreenInitializer() {}
}
