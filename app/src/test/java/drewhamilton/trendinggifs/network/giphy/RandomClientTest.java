package drewhamilton.trendinggifs.network.giphy;

import drewhamilton.trendinggifs.network.giphy.models.response.Gif;
import drewhamilton.trendinggifs.network.giphy.models.response.RandomResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import retrofit2.Call;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RandomClientTest {

    private static final Gif GIF = new Gif("id", "title", null);

    @Mock private GiphyApi mockApi;
    @Mock private Caller mockCaller;

    private RandomClient client;

    @Before
    public void setUp() {
        final Call<RandomResponse> mockCall = mock(Call.class);
        when(mockApi.random(Constants.API_KEY)).thenReturn(mockCall);

        final RandomResponse response = new RandomResponse(GIF);
        when(mockCaller.execute(mockCall)).thenReturn(response);

        client = new RandomClient(mockApi, mockCaller);
    }

    @Test
    public void trending_returnsResultOfExecute() {
        RandomResponse result = client.random();
        verify(mockApi).random(Constants.API_KEY);
        assertEquals(GIF, result.gif);
    }
}
