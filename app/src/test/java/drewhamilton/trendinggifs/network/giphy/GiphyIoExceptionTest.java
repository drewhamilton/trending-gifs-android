package drewhamilton.trendinggifs.network.giphy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class GiphyIoExceptionTest {

    private static final String EXPECTED_MESSAGE = "IOException while making call to the Giphy API";

    @Mock private IOException mockIoException;

    @InjectMocks private GiphyIoException giphyIoException;

    @Test
    public void hasCorrectMessageAndCause() {
        assertEquals(EXPECTED_MESSAGE, giphyIoException.getMessage());
        assertEquals(mockIoException, giphyIoException.getCause());
    }
}
