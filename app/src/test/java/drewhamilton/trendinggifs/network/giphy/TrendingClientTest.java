package drewhamilton.trendinggifs.network.giphy;

import drewhamilton.trendinggifs.network.giphy.models.response.Gif;
import drewhamilton.trendinggifs.network.giphy.models.response.Pagination;
import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import retrofit2.Call;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrendingClientTest {

    private static final List<Gif> GIF_LIST = Collections.singletonList(new Gif("id", "title", null));
    private static final Pagination PAGINATION = new Pagination(6, 7, 8);

    @Mock private GiphyApi mockApi;
    @Mock private Caller mockCaller;

    private TrendingClient client;

    @Before
    public void setUp() {
        final Call<TrendingResponse> mockCall = mock(Call.class);
        when(mockApi.trending(eq(Constants.API_KEY), anyInt())).thenReturn(mockCall);

        final TrendingResponse response = new TrendingResponse(GIF_LIST, PAGINATION);
        when(mockCaller.execute(mockCall)).thenReturn(response);

        client = new TrendingClient(mockApi, mockCaller);
    }

    @Test
    public void trending_returnsResultOfExecute() {
        TrendingResponse result = client.trending(234);
        verify(mockApi).trending(Constants.API_KEY, 234);
        assertEquals(GIF_LIST, result.gifs);
        assertEquals(PAGINATION, result.pagination);
    }
}
