package drewhamilton.trendinggifs.network.giphy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CallerTest {

    @Mock private Call<String> mockCall;

    private Caller caller;

    @Before
    public void setUp() {
        caller = new Caller();
    }

    @Test
    public void execute_returnsValidResponse() throws IOException {
        final String body = "Success!";
        final Response<String> mockResponse = mock(Response.class);
        when(mockResponse.body()).thenReturn(body);
        when(mockCall.execute()).thenReturn(mockResponse);

        String result = caller.execute(mockCall);
        assertEquals(body, result);
    }

    @Test
    public void execute_throwsIOException_convertsToGiphyIoException() throws IOException {
        IOException ioException = new IOException("Test exception");
        when(mockCall.execute()).thenThrow(ioException);

        try {
            caller.execute(mockCall);
            fail("Expected GiphyIoException to be thrown");
        } catch (GiphyIoException result) {
            assertSame(ioException, result.getCause());
        }
    }
}
