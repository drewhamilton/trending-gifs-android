package drewhamilton.trendinggifs.network.tools;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringIntAdapterTest {

    private StringIntAdapter adapter;

    @Before
    public void setUp() {
        adapter = new StringIntAdapter();
    }

    @Test
    public void toJson_convertsIntToString() {
        assertEquals("234", adapter.toJson(234));
        assertEquals("-82435", adapter.toJson(-82435));
        assertEquals("0", adapter.toJson(0));
    }

    @Test
    public void fromJson_validInput_convertsStringToInt() {
        assertEquals(82734, adapter.fromJson("82734"));
        assertEquals(-838, adapter.fromJson("-838"));
        assertEquals(0, adapter.fromJson("0"));
    }

    @Test(expected = NumberFormatException.class)
    public void fromJson_invalidInput_throwsNumberFormatException() {
        adapter.fromJson("NaN");
    }
}
