package drewhamilton.trendinggifs.threading;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;

import static org.junit.Assert.assertSame;

public class AppSchedulersTest {

    private AppSchedulers appSchedulers;

    private Scheduler dummyMainThread = new TestScheduler();

    @Before
    public void setUp() {
        // Use dummy main thread to avoid depending on real Android thread:
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> dummyMainThread);

        appSchedulers = new AppSchedulers();
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.reset();
    }

    @Test
    public void ioScheduler_isSchedulersIo() {
        assertSame(Schedulers.io(), appSchedulers.io());
    }

    @Test
    public void uiScheduler_isAndroidMainThread() {
        assertSame(AndroidSchedulers.mainThread(), appSchedulers.ui());
    }
}
