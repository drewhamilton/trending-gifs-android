package drewhamilton.trendinggifs.data;

import drewhamilton.trendinggifs.data.models.AppModelFactory;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.data.models.PreviewPage;
import drewhamilton.trendinggifs.network.giphy.TrendingClient;
import drewhamilton.trendinggifs.network.giphy.models.response.Gif;
import drewhamilton.trendinggifs.network.giphy.models.response.TrendingResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrendingGifsRetrieverTest {

    private static final int LIST_SIZE = 87;

    @Mock private TrendingClient mockClient;
    @Mock private AppModelFactory mockModelFactory;

    private TrendingGifsRetriever trendingGifsRetriever;

    private TrendingResponse testResponse;

    @Before
    public void setUp() {
        testResponse = createResponse();

        when(mockClient.trending(anyInt())).thenReturn(testResponse);
        when(mockModelFactory.newPreviewPage(any())).thenAnswer(invocation -> convert((TrendingResponse) invocation.getArgument(0)));

        trendingGifsRetriever = new TrendingGifsRetriever(mockClient, mockModelFactory);
    }

    @Test
    public void retrieveTrendingGifs_returnsConvertedGifs() {
        final int startIndex = 24234;
        PreviewPage result = trendingGifsRetriever.retrieveTrendingGifs(startIndex);
        verify(mockClient).trending(startIndex);
        verifyNoMoreInteractions(mockClient);
        assertNotNull(result);
        assertEquals(LIST_SIZE, result.gifItems.size());
        for (int i = 0; i < LIST_SIZE; ++i) {
            assertEquals(testResponse.gifs.get(i).id, result.gifItems.get(i).title);
        }
    }

    private static TrendingResponse createResponse() {
        List<Gif> list = new ArrayList<>(LIST_SIZE);
        for (int i = 0; i < LIST_SIZE; ++i) {
            list.add(new Gif(String.valueOf(i), null, null));
        }
        return new TrendingResponse(list, null);
    }

    private static PreviewPage convert(TrendingResponse response) {
        List<GifItem> gifItems = new ArrayList<>(response.gifs.size());
        for (Gif gif : response.gifs) {
            gifItems.add(convert(gif));
        }
        return new PreviewPage(gifItems, 0, 0);
    }

    private static GifItem convert(Gif gif) {
        return new GifItem(gif.id, null, 0, 0, null, 0, 0);
    }
}
