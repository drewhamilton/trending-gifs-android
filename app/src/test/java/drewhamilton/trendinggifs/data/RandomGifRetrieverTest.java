package drewhamilton.trendinggifs.data;

import drewhamilton.trendinggifs.data.models.AppModelFactory;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.network.giphy.RandomClient;
import drewhamilton.trendinggifs.network.giphy.models.response.Gif;
import drewhamilton.trendinggifs.network.giphy.models.response.RandomResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RandomGifRetrieverTest {

    private static final String TEST_TITLE = "Test title";

    @Mock private RandomClient mockClient;
    @Mock private AppModelFactory mockModelFactory;

    private RandomGifRetriever randomGifRetriever;

    private RandomResponse testResponse;

    @Before
    public void setUp() {
        testResponse = createResponse();

        when(mockClient.random()).thenReturn(testResponse);
        when(mockModelFactory.newPreviewImage(any())).thenAnswer(invocation -> convert(invocation.getArgument(0)));

        randomGifRetriever = new RandomGifRetriever(mockClient, mockModelFactory);
    }

    @Test
    public void retrieveRandomGif_returnsConvertedGifItem() {
        GifItem result = randomGifRetriever.retrieveRandomGif();
        assertNotNull(result);
        assertEquals(convert(testResponse.gif).title, result.title);
    }

    private static RandomResponse createResponse() {
        return new RandomResponse(new Gif(String.valueOf(TEST_TITLE), null, null));
    }

    private static GifItem convert(Gif gif) {
        return new GifItem(gif.id, null, 0, 0, null, 0, 0);
    }
}
