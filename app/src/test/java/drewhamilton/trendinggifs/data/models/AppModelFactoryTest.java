package drewhamilton.trendinggifs.data.models;

import drewhamilton.trendinggifs.network.giphy.models.response.*;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AppModelFactoryTest {

    private static final String TEST_ID = "Test ID";
    private static final String TEST_TITLE = "Test title";

    private AppModelFactory appModelFactory;

    @Before
    public void setUp() {
        appModelFactory = new AppModelFactory();
    }

    @Test
    public void newPreviewPage_keepsExpectedProperties() {
        final String testStillUrl = "Test still URL";
        final int testStillWidth = 982374923;
        final int testStillHeight = -97234;
        final FixedWidthStillImage testFixedWidthStill = new FixedWidthStillImage(testStillUrl, testStillWidth, testStillHeight);

        final String testWebPUrl = "Test animated URL";
        final int testWebPWidth = -2387;
        final int testWebPHeight = 9823;
        final OriginalImage testOriginal = new OriginalImage(testWebPUrl, testWebPWidth, testWebPHeight);

        final Images testImages = new Images(testFixedWidthStill, testOriginal);
        final Gif inputGif = new Gif(TEST_ID, TEST_TITLE, testImages);

        final Gif errorGif = new Gif(null, null, null);

        final int testOffset = 3;
        final int testCount = 234;
        final int testTotalCount = 41234;
        final Pagination inputPagination = new Pagination(testOffset, testCount, testTotalCount);

        final TrendingResponse input = new TrendingResponse(Arrays.asList(errorGif, inputGif), inputPagination);
        PreviewPage result = appModelFactory.newPreviewPage(input);

        assertNotNull(result);
        assertEquals(testCount, result.size);
        assertEquals(testOffset, result.startIndex);

        assertEquals(1, result.gifItems.size());
        GifItem resultGifItem = result.gifItems.get(0);
        assertEquals(TEST_TITLE, resultGifItem.title);
        assertEquals(testStillUrl, resultGifItem.stillUrl);
        assertEquals(testStillWidth, resultGifItem.stillWidth);
        assertEquals(testStillHeight, resultGifItem.stillHeight);
        assertEquals(testWebPUrl, resultGifItem.webPUrl);
        assertEquals(testWebPWidth, resultGifItem.webPWidth);
        assertEquals(testWebPHeight, resultGifItem.webPHeight);
    }

    @Test
    public void newPreviewImage_keepsExpectedProperties() {
        final String testStillUrl = "Test still URL";
        final int testStillWidth = 123;
        final int testStillHeight = 234;
        final FixedWidthStillImage testFixedWidthStill = new FixedWidthStillImage(testStillUrl, testStillWidth, testStillHeight);

        final String testWebPUrl = "Test animated URL";
        final int testWebPWidth = 345;
        final int testWebPHeight = 456;
        final OriginalImage testOriginal = new OriginalImage(testWebPUrl, testWebPWidth, testWebPHeight);

        final Images testImages = new Images(testFixedWidthStill, testOriginal);
        final Gif inputGif = new Gif(TEST_ID, TEST_TITLE, testImages);

        final GifItem result = appModelFactory.newPreviewImage(inputGif);

        assertNotNull(result);
        assertEquals(TEST_TITLE, result.title);
        assertEquals(testStillUrl, result.stillUrl);
        assertEquals(testStillWidth, result.stillWidth);
        assertEquals(testStillHeight, result.stillHeight);
        assertEquals(testWebPUrl, result.webPUrl);
        assertEquals(testWebPWidth, result.webPWidth);
        assertEquals(testWebPHeight, result.webPHeight);
    }
}
