package drewhamilton.trendinggifs.ui.random;

import drewhamilton.trendinggifs.data.RandomGifRetriever;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.ui.base.BasePresenterTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RandomGifsPresenterTest extends BasePresenterTest<RandomGifsView, RandomGifsPresenter> {

    private static final int EXPECTED_POLLING_FREQUENCY_SECONDS = 10;
    private static final int EXPECTED_RETRIES = 2;

    private static final AnimatedViewModel TEST_VIEW_MODEL_1 = new AnimatedViewModel(getDescription(1), getUrl(1));
    private static final AnimatedViewModel TEST_VIEW_MODEL_2 = new AnimatedViewModel(getDescription(2), getUrl(2));
    private static final AnimatedViewModel ERROR_VIEW_MODEL = new ErrorAnimatedViewModel();

    private static final RandomGifsScreenInitializationInformation INITIALIZATION_INFORMATION
            = new RandomGifsScreenInitializationInformation(TEST_VIEW_MODEL_1);

    @Mock private RandomGifsScreenInitializer mockInitializer;
    @Mock private RandomGifRetriever mockRetriever;

    private RandomGifsPresenter presenter;

    @Captor private ArgumentCaptor<AnimatedViewModel> viewModelCaptor;

    @Override
    protected Class<RandomGifsView> getViewClass() {
        return RandomGifsView.class;
    }

    @Override
    protected RandomGifsPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setUp() {
        super.setUp();

        when(mockInitializer.retrieve())
                .thenReturn(INITIALIZATION_INFORMATION)
                .thenReturn(null);

        final GifItem testRandomGif = new GifItem(TEST_VIEW_MODEL_2.description, null, 2, 2, TEST_VIEW_MODEL_2.animatedImageUrl, 2, 2);
        when(mockRetriever.retrieveRandomGif()).thenReturn(testRandomGif);

        presenter = new RandomGifsPresenter(mockInitializer, getMockSchedulers(), mockRetriever);
    }

    @Test
    public void attachView_runsOnExpectedThreads() {
        getPresenter().attachView(getMockView());

        verify(getMockView()).displayNewGif(TEST_VIEW_MODEL_1);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestIoScheduler().advanceTimeBy(EXPECTED_POLLING_FREQUENCY_SECONDS - 1, TimeUnit.SECONDS);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestIoScheduler().advanceTimeBy(1, TimeUnit.SECONDS);
        verifyNoMoreInteractions(getMockView());
        verify(mockRetriever).retrieveRandomGif();
        verifyNoMoreInteractions(mockRetriever);

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(mockRetriever);
        verify(getMockView(), times(2)).displayNewGif(viewModelCaptor.capture());
        final AnimatedViewModel viewModel = viewModelCaptor.getValue();
        assertEquals(TEST_VIEW_MODEL_2.description, viewModel.description);
        assertEquals(TEST_VIEW_MODEL_2.animatedImageUrl, viewModel.animatedImageUrl);
    }

    @Test
    public void attachView_retrievalError_displaysErrorViewModel() {
        when(mockRetriever.retrieveRandomGif()).thenThrow(new RuntimeException("Test error"));

        getPresenter().attachView(getMockView());
        verify(getMockView()).displayNewGif(TEST_VIEW_MODEL_1);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestIoScheduler().advanceTimeBy(EXPECTED_POLLING_FREQUENCY_SECONDS, TimeUnit.SECONDS);
        verifyNoMoreInteractions(getMockView());
        verify(mockRetriever, times(1 + EXPECTED_RETRIES)).retrieveRandomGif();
        verifyNoMoreInteractions(mockRetriever);

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(mockRetriever);
        verify(getMockView(), times(2)).displayNewGif(viewModelCaptor.capture());
        final AnimatedViewModel viewModel = viewModelCaptor.getValue();
        assertEquals(ERROR_VIEW_MODEL.description, viewModel.description);
        assertEquals(ERROR_VIEW_MODEL.animatedImageUrl, viewModel.animatedImageUrl);
    }

    @Test
    public void attachView_nullInitializationInformation_displaysErrorViewModel() {
        when(mockInitializer.retrieve()).thenReturn(null);

        getPresenter().attachView(getMockView());
        verify(getMockView()).displayNewGif(viewModelCaptor.capture());
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        final AnimatedViewModel viewModel = viewModelCaptor.getValue();
        assertEquals(ERROR_VIEW_MODEL.description, viewModel.description);
        assertEquals(ERROR_VIEW_MODEL.animatedImageUrl, viewModel.animatedImageUrl);
    }

    @Test
    public void resumeView_resumesDisplayingImages() {
        getPresenter().attachView(getMockView());
        getPresenter().pauseView();

        verify(getMockView()).displayNewGif(TEST_VIEW_MODEL_1);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getPresenter().resumeView();
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestIoScheduler().advanceTimeBy(EXPECTED_POLLING_FREQUENCY_SECONDS, TimeUnit.SECONDS);
        verifyNoMoreInteractions(getMockView());
        verify(mockRetriever).retrieveRandomGif();
        verifyNoMoreInteractions(mockRetriever);

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(mockRetriever);
        verify(getMockView(), times(2)).displayNewGif(viewModelCaptor.capture());
        final AnimatedViewModel viewModel = viewModelCaptor.getValue();
        assertEquals(TEST_VIEW_MODEL_2.description, viewModel.description);
        assertEquals(TEST_VIEW_MODEL_2.animatedImageUrl, viewModel.animatedImageUrl);
    }

    @Test
    public void pauseView_stopsRetrievingImages() {
        getPresenter().attachView(getMockView());
        verify(getMockView()).displayNewGif(TEST_VIEW_MODEL_1);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getPresenter().pauseView();
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestIoScheduler().advanceTimeBy(EXPECTED_POLLING_FREQUENCY_SECONDS, TimeUnit.SECONDS);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(getMockView());
        verifyNoMoreInteractions(mockRetriever);
    }

    private static String getDescription(int number) {
        return String.format(Locale.US, "Description %d", number);
    }

    private static String getUrl(int number) {
        return String.format(Locale.US, "URL %d", number);
    }
}
