package drewhamilton.trendinggifs.ui.random;

import drewhamilton.trendinggifs.ui.base.BaseScreenInitializerTest;
import drewhamilton.trendinggifs.ui.base.ScreenInitializer;

public class RandomGifsScreenInitializerTest extends BaseScreenInitializerTest<RandomGifsScreenInitializationInformation> {

    private RandomGifsScreenInitializer initializer;

    @Override
    public void setUp() {
        super.setUp();
        initializer = new RandomGifsScreenInitializer();
    }

    @Override
    protected Class<RandomGifsScreenInitializationInformation> getInitializationInformationClass() {
        return RandomGifsScreenInitializationInformation.class;
    }

    @Override
    protected ScreenInitializer<RandomGifsScreenInitializationInformation> getScreenInitializer() {
        return initializer;
    }
}
