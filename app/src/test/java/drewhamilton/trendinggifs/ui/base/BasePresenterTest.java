package drewhamilton.trendinggifs.ui.base;

import android.support.annotation.CallSuper;

import org.junit.Before;
import org.junit.Test;

import drewhamilton.trendinggifs.threading.AppSchedulers;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.TestScheduler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class BasePresenterTest<V extends MvpView, P extends Presenter<V>> {

    private V mockView;

    private AppSchedulers mockSchedulers;

    private TestScheduler testIoScheduler = new TestScheduler();
    private TestScheduler testUiScheduler = new TestScheduler();

    @Before
    @CallSuper
    public void setUp() {
        mockView = mock(getViewClass());

        mockSchedulers = mock(AppSchedulers.class);
        when(mockSchedulers.io()).thenReturn(testIoScheduler);
        when(mockSchedulers.ui()).thenReturn(testUiScheduler);
    }

    @Test
    public void attachView_retainsView() {
        getPresenter().attachView(getMockView());

        assertTrue(getPresenter().isViewAttached());
        assertEquals(getMockView(), getPresenter().getView());
    }

    @Test
    public void detachView_forgetsView() {
        getPresenter().attachView(getMockView());
        getPresenter().detachView();

        assertFalse(getPresenter().isViewAttached());
        assertNull(getPresenter().getView());
    }

    @Test
    public void detachView_disposesAddedSubscription() {
        getPresenter().attachView(getMockView());

        Disposable testSubscription = Completable.complete()
                .subscribeOn(testIoScheduler)
                .subscribe();
        getPresenter().addSubscription(testSubscription);
        assertFalse(testSubscription.isDisposed());

        getPresenter().detachView();
        assertTrue(testSubscription.isDisposed());
    }

    protected abstract Class<V> getViewClass();

    protected abstract P getPresenter();

    protected V getMockView() {
        return mockView;
    }

    protected AppSchedulers getMockSchedulers() {
        return mockSchedulers;
    }

    protected TestScheduler getTestIoScheduler() {
        return testIoScheduler;
    }

    protected TestScheduler getTestUiScheduler() {
        return testUiScheduler;
    }
}
