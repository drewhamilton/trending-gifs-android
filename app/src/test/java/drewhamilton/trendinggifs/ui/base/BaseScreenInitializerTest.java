package drewhamilton.trendinggifs.ui.base;

import android.support.annotation.CallSuper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;

public abstract class BaseScreenInitializerTest<I extends InitializationInformation> {

    private I mockInitializationInformation;

    protected abstract Class<I> getInitializationInformationClass();

    protected abstract ScreenInitializer<I> getScreenInitializer();

    @Before
    @CallSuper
    public void setUp() {
        mockInitializationInformation = mock(getInitializationInformationClass());
    }

    @Test
    public void retrieve_initiallyReturnsNull() {
        assertNull(getScreenInitializer().retrieve());
    }

    @Test
    public void provideAndRetrieve_setsInformationToNull() {
        getScreenInitializer().provide(mockInitializationInformation);
        final I result = getScreenInitializer().retrieve();
        assertSame(mockInitializationInformation, result);

        assertNull(getScreenInitializer().retrieve());
    }
}
