package drewhamilton.trendinggifs.ui.trending;

import drewhamilton.trendinggifs.data.TrendingGifsRetriever;
import drewhamilton.trendinggifs.data.models.GifItem;
import drewhamilton.trendinggifs.data.models.PreviewPage;
import drewhamilton.trendinggifs.network.giphy.GiphyIoException;
import drewhamilton.trendinggifs.ui.base.BasePresenterTest;
import drewhamilton.trendinggifs.ui.random.AnimatedViewModel;
import drewhamilton.trendinggifs.ui.random.RandomGifsScreenInitializationInformation;
import drewhamilton.trendinggifs.ui.random.RandomGifsScreenInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TrendingGifsPresenterTest extends BasePresenterTest<TrendingGifsView, TrendingGifsPresenter> {

    private static final int PAGE_SIZE = 348;

    private static final String TEST_TITLE = "Title %d";
    private static final String TEST_STILL_URL = "Still URL %d";
    private static final String TEST_WEBP_URL = "WebP URL %d";

    private static final int EXPECTED_RETRY_COUNT = 3;

    @Mock private TrendingGifsRetriever mockRetriever;
    @Mock private RandomGifsScreenInitializer mockNextScreenInitializer;

    private TrendingGifsPresenter presenter;

    private PreviewPage testPreviewPage1;

    @Captor private ArgumentCaptor<List<PreviewViewModel>> viewModelCaptor;

    @Override
    protected Class<TrendingGifsView> getViewClass() {
        return TrendingGifsView.class;
    }

    @Override
    protected TrendingGifsPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setUp() {
        super.setUp();

        testPreviewPage1 = createPreviewPage(0);
        final PreviewPage testPreviewPage2 = createPreviewPage(1);
        when(mockRetriever.retrieveTrendingGifs(0)).thenReturn(testPreviewPage1);
        when(mockRetriever.retrieveTrendingGifs(PAGE_SIZE)).thenReturn(testPreviewPage2);

        presenter = new TrendingGifsPresenter(getMockSchedulers(), mockRetriever, mockNextScreenInitializer);
    }

    @Test
    public void attachView_runsOnExpectedThreads() {
        presenter.attachView(getMockView());
        verifyNoMoreInteractions(mockRetriever);
        verifyNoMoreInteractions(getMockView());

        getTestIoScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(mockRetriever).retrieveTrendingGifs(0);
        verifyNoMoreInteractions(mockRetriever);
        verifyNoMoreInteractions(getMockView());

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(mockRetriever);
        verify(getMockView()).addItemsToDisplay(viewModelCaptor.capture());
        verifyNoMoreInteractions(getMockView());

        final List<PreviewViewModel> displayedList = viewModelCaptor.getValue();
        assertNotNull(displayedList);
        assertEquals(PAGE_SIZE, displayedList.size());
        for (int i = 0; i < PAGE_SIZE; ++i) {
            assertEquals(getTitle(i), displayedList.get(i).description);
            assertEquals(getStillUrl(i), displayedList.get(i).previewImageUrl);
        }
    }

    @Test
    public void attachView_handlesError() {
        when(mockRetriever.retrieveTrendingGifs(0)).thenThrow(new GiphyIoException(new IOException("Test exception")));

        presenter.attachView(getMockView());

        getTestIoScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(mockRetriever, times(1 + EXPECTED_RETRY_COUNT)).retrieveTrendingGifs(0);

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(getMockView(), never()).addItemsToDisplay(anyList());
        verify(getMockView()).displayEndOfItems();
    }

    @Test
    public void requestMorePreviews_getsNextPage() {
        presenter.attachView(getMockView());
        getTestIoScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);

        verify(mockRetriever).retrieveTrendingGifs(anyInt());
        verify(getMockView()).addItemsToDisplay(anyList());
        verifyNoMoreInteractions(mockRetriever);
        verifyNoMoreInteractions(getMockView());

        presenter.requestMorePreviews();
        verifyNoMoreInteractions(mockRetriever);
        verifyNoMoreInteractions(getMockView());

        getTestIoScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(mockRetriever).retrieveTrendingGifs(PAGE_SIZE);
        verifyNoMoreInteractions(mockRetriever);
        verifyNoMoreInteractions(getMockView());

        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(getMockView(), times(2)).addItemsToDisplay(viewModelCaptor.capture());
        final List<PreviewViewModel> displayedList = viewModelCaptor.getValue();
        assertNotNull(displayedList);
        assertEquals(PAGE_SIZE, displayedList.size());
        for (int i = 0; i < PAGE_SIZE; ++i) {
            assertEquals(getTitle(i + PAGE_SIZE), displayedList.get(i).description);
            assertEquals(getStillUrl(i + PAGE_SIZE), displayedList.get(i).previewImageUrl);
        }
    }

    @Test
    public void launchSingleGifView_passesInfoToNextScreen() {
        presenter.attachView(getMockView());
        getTestIoScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        getTestUiScheduler().advanceTimeBy(1, TimeUnit.MILLISECONDS);
        verify(getMockView()).addItemsToDisplay(anyList());
        verifyNoMoreInteractions(getMockView());

        final GifItem testItem = testPreviewPage1.gifItems.get(PAGE_SIZE / 2);
        final PreviewViewModel testInput = convertToPreviewViewModel(testItem);
        final AnimatedViewModel expectedOutput = convertToAnimatedViewModel(testItem);

        presenter.launchSingleGifView(testInput);

        final ArgumentCaptor<RandomGifsScreenInitializationInformation> initializationInformationCaptor
                = ArgumentCaptor.forClass(RandomGifsScreenInitializationInformation.class);
        verify(mockNextScreenInitializer).provide(initializationInformationCaptor.capture());
        verify(getMockView()).launchRandomGifsScreen();
        verifyNoMoreInteractions(mockNextScreenInitializer);
        verifyNoMoreInteractions(getMockView());

        final RandomGifsScreenInitializationInformation initializationInformation = initializationInformationCaptor.getValue();
        assertEquals(expectedOutput.description, initializationInformation.viewModel.description);
        assertEquals(expectedOutput.animatedImageUrl, initializationInformation.viewModel.animatedImageUrl);
    }

    private static PreviewPage createPreviewPage(int pageIndex) {
        return new PreviewPage(createAppModelList(pageIndex), 0, PAGE_SIZE);
    }

    private static List<GifItem> createAppModelList(int pageIndex) {
        List<GifItem> list = new ArrayList<>(PAGE_SIZE);
        for (int i = pageIndex * PAGE_SIZE; i < (pageIndex + 1) * PAGE_SIZE; ++i) {
            GifItem gifItem = new GifItem(getTitle(i), getStillUrl(i), i, i, getWebpUrl(i), i, i);
            list.add(gifItem);
        }

        return list;
    }

    private static PreviewViewModel convertToPreviewViewModel(GifItem gifItem) {
        return new PreviewViewModel(gifItem.title, gifItem.stillUrl);
    }

    private static AnimatedViewModel convertToAnimatedViewModel(GifItem gifItem) {
        return new AnimatedViewModel(gifItem.title, gifItem.webPUrl);
    }

    private static String getTitle(int index) {
        return String.format(Locale.US, TEST_TITLE, index);
    }

    private static String getStillUrl(int index) {
        return String.format(Locale.US, TEST_STILL_URL, index);
    }

    private static String getWebpUrl(int index) {
        return String.format(Locale.US, TEST_WEBP_URL, index);
    }
}
