package drewhamilton.trendinggifs.ui.images;

import android.graphics.drawable.Animatable;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import drewhamilton.trendinggifs.R;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ImageLoaderTest {

    private static final int EXPECTED_FALLBACK_DRAWABLE = R.drawable.ic_error_white_24dp;

    private static final String TEST_URL = "Test URL";

    @Mock private GenericDraweeHierarchy mockHierarchy;
    @Mock private DraweeController mockInitialController;
    @Mock private DraweeController mockNewController;

    @Mock private GenericDraweeView mockView;
    @Mock private ControllerListener<ImageInfo> mockListener;

    @Mock private ControllerFactory mockControllerFactory;

    @InjectMocks private ImageLoader imageLoader;

    @Captor private ArgumentCaptor<ControllerListener<ImageInfo>> listenerCaptor;

    @Before
    public void setUp() {
        when(mockView.getHierarchy()).thenReturn(mockHierarchy);
        when(mockView.getController()).thenReturn(mockInitialController);

        when(mockControllerFactory.buildDraweeController(eq(TEST_URL), same(mockInitialController), anyBoolean(), any()))
                .thenReturn(mockNewController);
    }

    @Test
    public void loadPreviewImage_withListener() {
        imageLoader.loadPreviewImage(mockView, TEST_URL, mockListener);

        ControllerListener<ImageInfo> listener = verifyAllCalls(ScalingUtils.ScaleType.CENTER_CROP, false);
        verifyListener(listener, false);
    }

    @Test
    public void loadPreviewImage_nullListener() {
        imageLoader.loadPreviewImage(mockView, TEST_URL, null);

        ControllerListener<ImageInfo> listener = verifyAllCalls(ScalingUtils.ScaleType.CENTER_CROP, false);
        verifyListener(listener, true);
    }

    @Test
    public void loadAnimatedImage_withListener() {
        imageLoader.loadAnimatedImage(mockView, TEST_URL, mockListener);

        ControllerListener<ImageInfo> listener = verifyAllCalls(ScalingUtils.ScaleType.FIT_CENTER, true);
        verifyListener(listener, false);
    }

    @Test
    public void loadAnimatedImage_nullListener() {
        imageLoader.loadAnimatedImage(mockView, TEST_URL, null);

        ControllerListener<ImageInfo> listener = verifyAllCalls(ScalingUtils.ScaleType.FIT_CENTER, true);
        verifyListener(listener, true);
    }

    private ControllerListener<ImageInfo> verifyAllCalls(ScalingUtils.ScaleType actualScaleType, boolean isAnimated) {
        verify(mockHierarchy).setFailureImage(EXPECTED_FALLBACK_DRAWABLE, ScalingUtils.ScaleType.CENTER_INSIDE);
        verify(mockHierarchy).setActualImageScaleType(actualScaleType);

        verify(mockControllerFactory).buildDraweeController(eq(TEST_URL), same(mockInitialController), eq(isAnimated), listenerCaptor.capture());

        verify(mockView).setController(mockNewController);

        return listenerCaptor.getValue();
    }

    private void verifyListener(ControllerListener<ImageInfo> verifiedListener, boolean passedNull) {
        if (passedNull) {
            assertTrue(verifiedListener instanceof LoggingListener);
        } else {
            final String testId = "Test ID";
            final ImageInfo mockImageInfo = mock(ImageInfo.class);
            final Animatable mockAnimatable = mock(Animatable.class);
            verifiedListener.onFinalImageSet(testId, mockImageInfo, mockAnimatable);
            verify(mockListener).onFinalImageSet(testId, mockImageInfo, mockAnimatable);
        }
    }
}
