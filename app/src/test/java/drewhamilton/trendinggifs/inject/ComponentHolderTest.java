package drewhamilton.trendinggifs.inject;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class ComponentHolderTest {

    private final AppComponent mockAppComponent = mock(AppComponent.class);

    @Test
    public void setAppComponent() {
        ComponentHolder.setAppComponent(mockAppComponent);
        assertEquals(mockAppComponent, ComponentHolder.getAppComponent());
    }
}
