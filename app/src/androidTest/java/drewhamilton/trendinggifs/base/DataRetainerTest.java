package drewhamilton.trendinggifs.base;

import android.support.annotation.NonNull;
import drewhamilton.trendinggifs.test.BaseUiTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class DataRetainerTest extends BaseUiTest<TestDataRetainerActivity> {

    @NonNull
    @Override
    protected Class<TestDataRetainerActivity> getActivityClass() {
        return TestDataRetainerActivity.class;
    }

    @Test
    public void configurationChange_savesData() {
        final int collectionSize = 9822;
        final Map<String, String> data = new HashMap<>(collectionSize);
        for (int i = 0; i < collectionSize; ++i) {
            data.put(getKey(i), getValue(i));
        }
        final Map<String, String> inputCopy = new HashMap<>(data);

        launchActivity();
        runOnUiThread(() -> getActivity().setData(data));
        assertSame(data, getActivity().getData());
        assertEquals(inputCopy, getActivity().getData());

        simulateConfigurationChange();
        assertSame(data, getActivity().getData());
        assertEquals(inputCopy, getActivity().getData());
    }

    private static String getKey(int index) {
        return String.format(Locale.US, "Key %d", index);
    }

    private static String getValue(int index) {
        return String.format(Locale.US, "Value %d", index);
    }
}
