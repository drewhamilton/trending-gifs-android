package drewhamilton.trendinggifs.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import drewhamilton.trendinggifs.ui.base.DataRetainer;

import java.util.Map;

public class TestDataRetainerActivity extends AppCompatActivity {

    private DataRetainer<Map<String, String>> dataRetainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataRetainer = DataRetainer.initialize(getSupportFragmentManager(), DataRetainer.DEFAULT_TAG);
    }

    Map<String, String> getData() {
        return dataRetainer.getData();
    }

    void setData(Map<String, String> data) {
        dataRetainer.setData(data);
    }
}
