package drewhamilton.trendinggifs.test.tools.matchers;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class AppViewMatchers {

    private AppViewMatchers() {}

    public static Matcher<View> hasItemCount(final int expectedItemCount) {
        return new SpecificTypeSafeViewMatcher<RecyclerView>() {

            @NonNull
            @Override
            protected Class<RecyclerView> specificType() {
                return RecyclerView.class;
            }

            @Override
            protected boolean matchesSpecifically(@NonNull RecyclerView item) {
                final RecyclerView.Adapter adapter = item.getAdapter();
                return adapter != null && adapter.getItemCount() == expectedItemCount;
            }

            @Override
            protected void describeSpecificallyTo(@NonNull Description description) {
                description.appendText("with item count ").appendValue(expectedItemCount);
            }

            @Override
            protected void describeMismatchSpecifically(@NonNull RecyclerView item, @NonNull Description mismatchDescription) {
                final RecyclerView.Adapter adapter = item.getAdapter();
                if (adapter == null) {
                    mismatchDescription.appendText("adapter was null");
                } else {
                    mismatchDescription.appendText("had item count ").appendValue(adapter.getItemCount());
                }
            }
        };
    }

    public static Matcher<View> isRecyclerViewChildAtPosition(final @IdRes int recyclerViewId, final int position) {
        return new RecyclerViewChildMatcher(recyclerViewId) {

            @Override
            protected boolean matchesConfirmedChild(@NonNull RecyclerView parent, @NonNull View child) {
                return position == parent.getChildAdapterPosition(child);
            }

            @Override
            protected void describeSpecificallyTo(@NonNull Description description) {
                description
                        .appendText("at position ")
                        .appendValue(position);
            }

            @Override
            protected void describeMismatchWithConfirmedChild(@NonNull RecyclerView parent, @NonNull View child,
                    @NonNull Description mismatchDescription) {
                mismatchDescription
                        .appendText("was at position ")
                        .appendValue(parent.getChildAdapterPosition(child));
            }
        };
    }

    public static Matcher<View> isLastVisibleRecyclerViewChild(final @IdRes int recyclerViewId) {
        return new RecyclerViewChildMatcher(recyclerViewId) {

            @Override
            protected boolean matchesConfirmedChild(@NonNull RecyclerView parent, @NonNull View child) {
                final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;

                    final int lastVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    final RecyclerView.ViewHolder viewHolder = parent.findViewHolderForAdapterPosition(lastVisibleItemPosition);
                    if (viewHolder != null) {
                        return child == viewHolder.itemView;
                    }
                }

                return false;
            }

            @Override
            protected void describeSpecificallyTo(@NonNull Description description) {
                description
                        .appendText("- the last visible child");
            }

            @Override
            protected void describeMismatchWithConfirmedChild(@NonNull RecyclerView parent, @NonNull View child,
                                                              @NonNull Description mismatchDescription) {
                final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
                if (!(layoutManager instanceof LinearLayoutManager)) {
                    mismatchDescription
                            .appendText("could not determine last visible child because the ")
                            .appendText("parent's LayoutManager was not a LinearLayoutManager");
                } else {
                    final int lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
                    final RecyclerView.ViewHolder viewHolder = parent.findViewHolderForAdapterPosition(lastVisibleItemPosition);
                    if (viewHolder == null) {
                        mismatchDescription
                                .appendText("could not determine last visible child because the ")
                                .appendText("ViewHolder at the last visible position was null");
                    } else {
                        mismatchDescription.appendText("was a different view than the last visible child view");
                    }
                }
            }
        };
    }

    public static Matcher<View> isLastCompletelyVisibleRecyclerViewChild(final @IdRes int recyclerViewId) {
        return new RecyclerViewChildMatcher(recyclerViewId) {

            @Override
            protected boolean matchesConfirmedChild(@NonNull RecyclerView parent, @NonNull View child) {
                final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;

                    final int lastVisibleItemPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    final RecyclerView.ViewHolder viewHolder = parent.findViewHolderForAdapterPosition(lastVisibleItemPosition);
                    if (viewHolder != null) {
                        return child == viewHolder.itemView;
                    }
                }

                return false;
            }

            @Override
            protected void describeSpecificallyTo(@NonNull Description description) {
                description
                        .appendText("- the last completely visible child");
            }

            @Override
            protected void describeMismatchWithConfirmedChild(@NonNull RecyclerView parent, @NonNull View child,
                    @NonNull Description mismatchDescription) {
                final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
                if (!(layoutManager instanceof LinearLayoutManager)) {
                    mismatchDescription
                            .appendText("could not determine last completely visible child because the ")
                            .appendText("parent's LayoutManager was not a LinearLayoutManager");
                } else {
                    final int lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
                    final RecyclerView.ViewHolder viewHolder = parent.findViewHolderForAdapterPosition(lastVisibleItemPosition);
                    if (viewHolder == null) {
                        mismatchDescription
                                .appendText("could not determine last completely visible child because the ")
                                .appendText("ViewHolder at the last completely visible position was null");
                    } else {
                        mismatchDescription.appendText("was a different view than the last visible child view");
                    }
                }
            }
        };
    }
}
