package drewhamilton.trendinggifs.test.tools.matchers;

import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewParent;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public abstract class RecyclerViewChildMatcher extends TypeSafeMatcher<View> {

    @IdRes private final int recyclerViewId;

    @Nullable private Resources resources;

    RecyclerViewChildMatcher(int recyclerViewId) {
        this.recyclerViewId = recyclerViewId;
    }

    @Override
    protected boolean matchesSafely(@NonNull View item) {
        resources = item.getResources();

        final ViewParent parent = item.getParent();
        if (parent instanceof RecyclerView && recyclerViewId == ((View) parent).getId()) {
            final RecyclerView recyclerView = (RecyclerView) parent;
            return matchesConfirmedChild(recyclerView, item);
        } else {
            return false;
        }
    }

    @Override
    public void describeTo(@NonNull Description description) {
        description
                .appendText("Child of RecyclerView ")
                .appendValue(getIdDescriptionValue(resources, recyclerViewId))
                .appendText(" ");
        describeSpecificallyTo(description);
    }

    @Override
    protected void describeMismatchSafely(@NonNull View item, @NonNull Description mismatchDescription) {
        final ViewParent parent = item.getParent();
        if (!(parent instanceof RecyclerView)) {
            mismatchDescription
                    .appendText("parent was an instance of ")
                    .appendText(parent.getClass().getSimpleName())
                    .appendText("instead of RecyclerView");
        } else if (recyclerViewId != ((View) parent).getId()) {
            mismatchDescription
                    .appendText("parent ID was ")
                    .appendValue(getIdDescriptionValue(item.getResources(), recyclerViewId));
        } else {
            describeMismatchWithConfirmedChild((RecyclerView) parent, item, mismatchDescription);
        }
    }

    /**
     * @param child is definitely a child View of the desired RecyclerView
     */
    protected abstract boolean matchesConfirmedChild(@NonNull RecyclerView parent, @NonNull View child);

    protected abstract void describeSpecificallyTo(@NonNull Description description);

    protected abstract void describeMismatchWithConfirmedChild(@NonNull RecyclerView parent, @NonNull View child,
            @NonNull Description mismatchDescription);

    protected static Object getIdDescriptionValue(Resources resources, @IdRes int id) {
        try {
            return resources.getResourceEntryName(id);
        } catch (NullPointerException | Resources.NotFoundException ex) {
            return id;
        }
    }
}
