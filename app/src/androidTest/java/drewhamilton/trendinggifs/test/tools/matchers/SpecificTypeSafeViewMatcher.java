package drewhamilton.trendinggifs.test.tools.matchers;

import android.support.annotation.NonNull;
import android.view.View;
import org.hamcrest.Description;

public abstract class SpecificTypeSafeViewMatcher<S extends View> extends SpecificTypeSafeMatcher<View, S> {

    @NonNull
    @Override
    protected abstract Class<S> specificType();

    @Override
    protected abstract boolean matchesSpecifically(@NonNull S item);

    @Override
    protected abstract void describeSpecificallyTo(@NonNull Description description);

    @Override
    protected abstract void describeMismatchSpecifically(@NonNull S item, @NonNull Description mismatchDescription);
}
