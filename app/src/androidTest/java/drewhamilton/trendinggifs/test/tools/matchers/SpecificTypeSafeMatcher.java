package drewhamilton.trendinggifs.test.tools.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public abstract class SpecificTypeSafeMatcher<T, S extends T> extends TypeSafeMatcher<T> {

    @Override
    protected final boolean matchesSafely(T item) {
        return specificType().isAssignableFrom(item.getClass())
                && matchesSpecifically(specificType().cast(item));
    }

    @Override
    public final void describeTo(Description description) {
        description
                .appendText(specificType().getSimpleName())
                .appendText(" ");
        describeSpecificallyTo(description);
    }

    @Override
    protected final void describeMismatchSafely(T item, Description mismatchDescription) {
        if (specificType().isAssignableFrom(item.getClass())) {
            describeMismatchSpecifically(specificType().cast(item), mismatchDescription);
        } else {
            mismatchDescription
                    .appendText("was an instance of ")
                    .appendText(item.getClass().getSimpleName())
                    .appendText("instead of ")
                    .appendText(specificType().getSimpleName());
        }
    }

    protected abstract Class<S> specificType();

    protected abstract boolean matchesSpecifically(S item);

    protected abstract void describeSpecificallyTo(Description description);

    protected abstract void describeMismatchSpecifically(S item, Description mismatchDescription);
}
