package drewhamilton.trendinggifs.inject;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import drewhamilton.trendinggifs.ui.images.ImageLoader;

import static org.mockito.Mockito.mock;

@Module
class TestImageModule {

    @Provides
    @Singleton
    ImageLoader provideImageLoader() {
        return mock(ImageLoader.class);
    }
}
