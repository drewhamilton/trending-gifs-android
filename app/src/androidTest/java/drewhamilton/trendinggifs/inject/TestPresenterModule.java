package drewhamilton.trendinggifs.inject;

import dagger.Module;
import dagger.Provides;
import drewhamilton.trendinggifs.ui.random.RandomGifsPresenter;
import drewhamilton.trendinggifs.ui.trending.TrendingGifsPresenter;

import javax.inject.Singleton;

import static org.mockito.Mockito.mock;

@Module
class TestPresenterModule {

    @Provides
    @Singleton
    RandomGifsPresenter provideRandomGifsPresenter() {
        return mock(RandomGifsPresenter.class);
    }

    @Provides
    @Singleton
    TrendingGifsPresenter provideTrendingGifsPresenter() {
        return mock(TrendingGifsPresenter.class);
    }
}
