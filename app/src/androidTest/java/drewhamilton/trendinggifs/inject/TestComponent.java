package drewhamilton.trendinggifs.inject;

import dagger.Component;
import drewhamilton.trendinggifs.ui.random.RandomGifsActivityTest;
import drewhamilton.trendinggifs.ui.trending.TrendingGifsActivityTest;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        TestImageModule.class,
        TestNetworkModule.class,
        TestPresenterModule.class
})
public interface TestComponent extends AppComponent {

    void inject(RandomGifsActivityTest randomGifsActivityTest);

    void inject(TrendingGifsActivityTest trendingGifsActivityTest);

}
