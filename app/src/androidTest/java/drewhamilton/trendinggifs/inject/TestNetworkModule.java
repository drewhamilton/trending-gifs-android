package drewhamilton.trendinggifs.inject;

import com.squareup.moshi.Moshi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import drewhamilton.trendinggifs.network.giphy.GiphyApi;
import drewhamilton.trendinggifs.network.tools.StringIntAdapter;
import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

@Module
class TestNetworkModule {

    @Provides
    @Singleton
    Moshi provideMoshi() {
        return new Moshi.Builder()
                .add(new StringIntAdapter())
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideGiphyRetrofit() {
        // Retrofit is final so it can't be mocked. But Retrofit should not be accessed in UI tests anyway
        return null;
    }

    @Provides
    @Singleton
    GiphyApi provideGiphyApi(Retrofit giphyRetrofit) {
        return mock(GiphyApi.class);
    }
}
