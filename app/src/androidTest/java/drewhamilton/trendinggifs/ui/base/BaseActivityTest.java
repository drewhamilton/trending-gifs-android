package drewhamilton.trendinggifs.ui.base;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatDelegate;
import drewhamilton.trendinggifs.test.BaseUiTest;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public abstract class BaseActivityTest<V extends MvpView, P extends Presenter<V>, A extends BaseActivity<V, P>>
        extends BaseUiTest<A> {

    @Test
    public void newPresenter_fromAppComponent() {
        launchActivity();
        assertEquals(getMockPresenter(), getActivity().newPresenter());
    }

    @Test
    public void onCreate_setsNightModeToYes() {
        runOnUiThread(() -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO));
        launchActivity();

        assertEquals(AppCompatDelegate.MODE_NIGHT_YES, AppCompatDelegate.getDefaultNightMode());
    }

    @Test
    public void onCreate_attachesToPresenter() {
        launchActivity();

        verify(getMockPresenter()).attachView((V) getActivity());
    }

    @Test
    public void onConfigurationChanges_doesNotDetach_reattachesToPresenter() {
        launchActivity();
        verify(getMockPresenter()).attachView((V) getActivity());

        simulateConfigurationChange();
        verify(getMockPresenter(), timeout(100)).replaceView((V) isA(BaseActivity.class));
    }

    @Test
    @Ignore // Not sure how to test this properly
    public void onDestroy_detachesFromPresenter() {
        launchActivity();
        verify(getMockPresenter()).attachView((V) getActivity());
        verifyNoMoreInteractions(getMockPresenter());

        runOnUiThread(() -> getActivity().onDestroy());
        waitForUiThread();

        verify(getMockPresenter()).detachView();
        verifyNoMoreInteractions(getMockPresenter());
    }

    @Test
    public void convertToDp_dividesPxByDensity() {
        launchActivity();

        final int inputPx = 28342;
        final int result = getActivity().convertToDp(inputPx);

        assertEquals((int) (inputPx / getActivity().getResources().getDisplayMetrics().density), result);
    }

    @Test
    public void getLayout_returnsExpectedLayout() {
        launchActivity();
        assertEquals(getExpectedLayout(), getActivity().getLayout());
    }

    protected abstract P getMockPresenter();

    @LayoutRes
    protected abstract int getExpectedLayout();
}
