package drewhamilton.trendinggifs.ui.random;

import android.support.annotation.NonNull;
import android.widget.ImageView;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.ui.base.BaseActivityTest;
import drewhamilton.trendinggifs.ui.images.ImageLoader;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import javax.inject.Inject;
import java.util.Locale;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.*;

public class RandomGifsActivityTest extends BaseActivityTest<RandomGifsView, RandomGifsPresenter, RandomGifsActivity> {

    private static final String TEST_DESCRIPTION = "Description %d";
    private static final String TEST_URL = "URL %d";

    @Inject ImageLoader mockImageLoader;

    @NonNull
    @Override
    protected Class<RandomGifsActivity> getActivityClass() {
        return RandomGifsActivity.class;
    }

    @Override
    protected RandomGifsPresenter getMockPresenter() {
        return getComponent().randomGifsPresenter();
    }

    @Override
    protected int getExpectedLayout() {
        return R.layout.activity_random_gifs;
    }

    @Override
    public void setUp() {
        super.setUp();
        getComponent().inject(this);
    }

    @Test
    public void onResume_resumesPresenter() {
        launchActivity();
        verify(getMockPresenter()).resumeView();
    }

    @Test
    public void onPause_pausesPresenter() {
        launchActivity();

        runOnUiThread(() -> getActivity().onPause());

        verify(getMockPresenter()).pauseView();
    }

    @Test
    public void displayNewGif_followsExpectedLoadingSequence() {
        final AnimatedViewModel testViewModel = new AnimatedViewModel(getDescription(1), getUrl(1));

        final ArgumentCaptor<GenericDraweeView> imageViewCaptor = ArgumentCaptor.forClass(GenericDraweeView.class);
        final ArgumentCaptor<ControllerListener<ImageInfo>> listenerCaptor = ArgumentCaptor.forClass(ControllerListener.class);

        launchActivity();

        onView(withText(testViewModel.description)).check(doesNotExist());
        onView(withId(R.id.progress_circle)).check(matches(isDisplayed()));

        runOnUiThread(() -> getActivity().displayNewGif(testViewModel));
        verify(mockImageLoader).loadAnimatedImage(imageViewCaptor.capture(), eq(testViewModel.animatedImageUrl), listenerCaptor.capture());

        final ImageView imageView = imageViewCaptor.getValue();
        assertEquals(R.id.random_gif_b, imageView.getId());

        onView(withText(testViewModel.description)).check(doesNotExist());
        onView(withId(R.id.progress_circle)).check(matches(isDisplayed()));
        onView(withId(R.id.random_gif_a)).check(matches(withAlpha(1f)));
        onView(withId(R.id.random_gif_b))
                .check(matches(withAlpha(0f)))
                .check(matches(withContentDescription(testViewModel.description)));

        final ControllerListener<ImageInfo> listener = listenerCaptor.getValue();
        runOnUiThread(() -> listener.onFinalImageSet("id", null, null));

        onView(withText(testViewModel.description)).check(matches(isDisplayed()));
        onView(withId(R.id.progress_circle)).check(matches(withEffectiveVisibility(Visibility.GONE)));
        onView(withId(R.id.random_gif_a)).check(matches(withAlpha(0f)));
        onView(withId(R.id.random_gif_b))
                .check(matches(withAlpha(1f)))
                .check(matches(withContentDescription(testViewModel.description)));
    }

    @Test
    public void displayNewGif_multipleCalls_alternatesImageViews() {
        final AnimatedViewModel testViewModel1 = new AnimatedViewModel(getDescription(1), getUrl(1));
        final AnimatedViewModel testViewModel2 = new AnimatedViewModel(getDescription(2), getUrl(2));
        final AnimatedViewModel testViewModel3 = new AnimatedViewModel(getDescription(3), getUrl(3));
        final AnimatedViewModel testViewModel4 = new AnimatedViewModel(getDescription(4), getUrl(4));

        final ArgumentCaptor<GenericDraweeView> imageViewCaptor = ArgumentCaptor.forClass(GenericDraweeView.class);
        final ArgumentCaptor<ControllerListener<ImageInfo>> listenerCaptor = ArgumentCaptor.forClass(ControllerListener.class);

        launchActivity();

        runOnUiThread(() -> getActivity().displayNewGif(testViewModel1));
        verify(mockImageLoader).loadAnimatedImage(imageViewCaptor.capture(), eq(testViewModel1.animatedImageUrl), listenerCaptor.capture());
        final GenericDraweeView imageView1 = imageViewCaptor.getValue();
        assertEquals(testViewModel1.description, imageView1.getContentDescription());
        final ControllerListener<ImageInfo> listener1 = listenerCaptor.getValue();
        runOnUiThread(() -> listener1.onFinalImageSet("id", null, null));

        runOnUiThread(() -> getActivity().displayNewGif(testViewModel2));
        verify(mockImageLoader).loadAnimatedImage(imageViewCaptor.capture(), eq(testViewModel2.animatedImageUrl), listenerCaptor.capture());
        final GenericDraweeView imageView2 = imageViewCaptor.getValue();
        assertEquals(testViewModel2.description, imageView2.getContentDescription());
        final ControllerListener<ImageInfo> listener2 = listenerCaptor.getValue();
        runOnUiThread(() -> listener2.onFinalImageSet("id", null, null));

        assertNotEquals(imageView1, imageView2);

        runOnUiThread(() -> getActivity().displayNewGif(testViewModel3));
        verify(mockImageLoader).loadAnimatedImage(same(imageView1), eq(testViewModel3.animatedImageUrl), listenerCaptor.capture());
        final ControllerListener<ImageInfo> listener3 = listenerCaptor.getValue();
        runOnUiThread(() -> listener3.onFinalImageSet("id", null, null));

        runOnUiThread(() -> getActivity().displayNewGif(testViewModel4));
        verify(mockImageLoader).loadAnimatedImage(same(imageView2), eq(testViewModel4.animatedImageUrl), any());
    }

    @Test
    public void onConfigurationChange_restoresActiveImage() {
        final AnimatedViewModel testViewModel = new AnimatedViewModel(getDescription(1), getUrl(1));

        final ArgumentCaptor<GenericDraweeView> imageViewCaptor = ArgumentCaptor.forClass(GenericDraweeView.class);
        final ArgumentCaptor<ControllerListener<ImageInfo>> listenerCaptor = ArgumentCaptor.forClass(ControllerListener.class);

        launchActivity();
        runOnUiThread(() -> getActivity().displayNewGif(testViewModel));
        verify(mockImageLoader).loadAnimatedImage(any(), eq(testViewModel.animatedImageUrl), listenerCaptor.capture());
        final ControllerListener<ImageInfo> listener = listenerCaptor.getValue();
        runOnUiThread(() -> listener.onFinalImageSet("id", null, null));
        waitForUiThread();
        verifyNoMoreInteractions(mockImageLoader);
        reset(mockImageLoader);

        simulateConfigurationChange();
        verify(mockImageLoader, timeout(200)).loadAnimatedImage(imageViewCaptor.capture(), eq(testViewModel.animatedImageUrl),
                listenerCaptor.capture());
        final ControllerListener<ImageInfo> listenerAfterConfigurationChange = listenerCaptor.getValue();
        runOnUiThread(() -> listenerAfterConfigurationChange.onFinalImageSet("id", null, null));

        onView(withText(testViewModel.description)).check(matches(isDisplayed()));
        onView(withId(imageViewCaptor.getValue().getId()))
                .check(matches(withAlpha(1f)))
                .check(matches(withContentDescription(testViewModel.description)));
    }

    private static String getDescription(int index) {
        return String.format(Locale.US, TEST_DESCRIPTION, index);
    }

    private static String getUrl(int index) {
        return String.format(Locale.US, TEST_URL, index);
    }
}
