package drewhamilton.trendinggifs.ui.trending;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.facebook.drawee.view.GenericDraweeView;
import drewhamilton.trendinggifs.R;
import drewhamilton.trendinggifs.ui.base.BaseActivityTest;
import drewhamilton.trendinggifs.ui.images.ImageLoader;
import drewhamilton.trendinggifs.ui.random.RandomGifsActivity;
import org.hamcrest.Matcher;
import org.junit.Test;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static drewhamilton.trendinggifs.test.tools.actions.AppViewActions.loopMainThread;
import static drewhamilton.trendinggifs.test.tools.matchers.AppViewMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;

public class TrendingGifsActivityTest extends BaseActivityTest<TrendingGifsView, TrendingGifsPresenter, TrendingGifsActivity> {

    private static final int LIST_SIZE_SMALL = 2;
    private static final int LIST_SIZE_LARGE = 20;

    private static final String TEST_DESCRIPTION = "Description %d";
    private static final String TEST_URL = "Test URL %d";

    @Inject ImageLoader mockImageLoader;

    @NonNull
    @Override
    protected Class<TrendingGifsActivity> getActivityClass() {
        return TrendingGifsActivity.class;
    }

    @Override
    protected TrendingGifsPresenter getMockPresenter() {
        return getComponent().trendingGifsPresenter();
    }

    @Override
    protected int getExpectedLayout() {
        return R.layout.activity_trending_gifs;
    }

    @Override
    public void setUp() {
        super.setUp();
        getComponent().inject(this);
    }

    @Test
    public void addItemsToDisplay_loadsEachImage() {
        launchActivity();
        runOnUiThread(() -> getActivity().addItemsToDisplay(createViewModels()));

        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .check(matches(hasItemCount(Integer.MAX_VALUE)));
        for (int i = 0; i < LIST_SIZE_SMALL; ++i) {
            verify(mockImageLoader).loadPreviewImage(any(GenericDraweeView.class), eq(getTestUrl(i)), isNull());
        }
    }

    @Test
    public void addItemsToDisplay_thenAddMore_loadsEachImage() {
        launchActivity();

        runOnUiThread(() -> getActivity().addItemsToDisplay(createViewModels()));
        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .check(matches(hasItemCount(Integer.MAX_VALUE)));
        for (int i = 0; i < LIST_SIZE_SMALL; ++i) {
            verify(mockImageLoader).loadPreviewImage(any(GenericDraweeView.class), eq(getTestUrl(i)), isNull());
        }
        verifyNoMoreInteractions(mockImageLoader);

        runOnUiThread(() -> getActivity().addItemsToDisplay(createViewModels(LIST_SIZE_SMALL)));
        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .check(matches(hasItemCount(Integer.MAX_VALUE)));
        for (int i = LIST_SIZE_SMALL; i < 2*LIST_SIZE_SMALL; ++i) {
            verify(mockImageLoader).loadPreviewImage(any(GenericDraweeView.class), eq(getTestUrl(i)), isNull());
        }
        verifyNoMoreInteractions(mockImageLoader);
    }

    @Test
    public void displayEndOfItems_limitsDisplayedListSize() {
        launchActivity();
        runOnUiThread(() -> getActivity().addItemsToDisplay(createViewModels()));
        runOnUiThread(() -> getActivity().displayEndOfItems());

        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .check(matches(hasItemCount(LIST_SIZE_SMALL)));
    }

    @Test
    public void launchRandomGifsScreen() {
        launchActivity();
        intending(hasComponent(RandomGifsActivity.class.getName()))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, new Intent()));

        runOnUiThread(() -> getActivity().launchRandomGifsScreen());

        intended(hasComponent(RandomGifsActivity.class.getName()));
    }

    @Test
    public void scroll_requestMoreItemsEvery25() {
        launchActivity();
        runOnUiThread(() -> getActivity().addItemsToDisplay(createViewModels(0, LIST_SIZE_LARGE)));

        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .perform(swipeUpAndWait())
                .perform(swipeUpAndWait())
                .perform(swipeUpAndWait())
                .perform(swipeUpAndWait())
                .perform(swipeUpAndWait())
                .perform(swipeUpAndWait());

        onView(isLastVisibleRecyclerViewChild(R.id.list))
                .perform(new ViewAction() {
                    @Override
                    public Matcher<View> getConstraints() {
                        return isLastVisibleRecyclerViewChild(R.id.list);
                    }

                    @Override
                    public String getDescription() {
                        return "Verify requestMorePreviews was called the expected number of times";
                    }

                    @Override
                    public void perform(UiController uiController, View view) {
                        final int boundItemCount = ((RecyclerView) view.getParent()).getChildAdapterPosition(view);
                        final int expectedRequestMoreInterval = 25;
                        final int expectedRequestCount = boundItemCount / expectedRequestMoreInterval;
                        Timber.v("expectedRequestCount: %d", expectedRequestCount);
                        verify(getMockPresenter(), atLeast(expectedRequestCount)).requestMorePreviews();
                        verify(getMockPresenter(), atMost(expectedRequestCount + 1)).requestMorePreviews();
                    }
                });
    }

    @Test
    public void clickItem_callsLaunchSingleGifView() {
        launchActivity();
        final List<PreviewViewModel> viewModels = createViewModels();
        runOnUiThread(() -> getActivity().addItemsToDisplay(viewModels));

        for (int i = 0; i < LIST_SIZE_SMALL; ++i) {
            onView(withContentDescription(getTestDescription(i)))
                    .check(matches(isDisplayed()))
                    .perform(click());
            verify(getMockPresenter()).launchSingleGifView(viewModels.get(i));
        }
    }

    @Test
    public void configurationChange_savesItemsAndPosition() {
        launchActivity();
        final List<PreviewViewModel> viewModels = createViewModels(0, 5*LIST_SIZE_LARGE);
        runOnUiThread(() -> getActivity().addItemsToDisplay(viewModels));

        onView(withId(R.id.list))
                .check(matches(isDisplayed()))
                .perform(swipeUp());
        waitForUiThread(3, TimeUnit.SECONDS);

        final ViewInteraction lastCompletelyVisibleItemInteraction = onView(isLastCompletelyVisibleRecyclerViewChild(R.id.list));
        lastCompletelyVisibleItemInteraction.check(matches(isDisplayed()));

        simulateConfigurationChange();

        lastCompletelyVisibleItemInteraction.check(matches(isDisplayed()));
    }

    private static List<PreviewViewModel> createViewModels() {
        return createViewModels(0);
    }

    private static List<PreviewViewModel> createViewModels(int startPosition) {
        return createViewModels(startPosition, LIST_SIZE_SMALL);
    }

    private static List<PreviewViewModel> createViewModels(int startPosition, int size) {
        List<PreviewViewModel> list = new ArrayList<>(size);
        for (int i = startPosition; i < startPosition + size; ++i) {
            list.add(new PreviewViewModel(getTestDescription(i), getTestUrl(i)));
        }
        return list;
    }

    private static String getTestDescription(int index) {
        return String.format(Locale.US, TEST_DESCRIPTION, index);
    }

    private static String getTestUrl(int index) {
        return String.format(Locale.US, TEST_URL, index);
    }

    private static ViewAction[] swipeUpAndWait() {
        return new ViewAction[] {swipeUp(), loopMainThread(1, TimeUnit.SECONDS)};
    }
}
